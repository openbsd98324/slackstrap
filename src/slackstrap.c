


/// Slackstrap for Slackware 
/// MIT Licensing, Free Software 
/// slackstrap.c <--- needs volunteers to make a cleaner C Code 
///  version 0.36 : with rootfs for kde
///


#include <stdio.h>
#include <stdlib.h>  //PATH_MAX in linux
#include <string.h>
#include <dirent.h>
#include <time.h>

#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif

#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h> 
#include <time.h>


#include <termios.h>
#define ESC "\033"



// Version: 0.22a
// history: 0.1   : basic without interpreter
//        : 0.21a : introduced interpreter
//        : 0.22a : introduced grep 
//        : 0.29 :  introduced fetch   
//        : 0.31 :  introduced install  
//        : 0.32 :  introduced installpkg  
//        : 0.33 :  introduced installpkg, install and confirmation  
//        : 0.34 :  First Release (stable) 
//        : 0.35, released and needs clean up of strncpy and system() 
///  version 0.35 : initial





char var_repository[PATH_MAX]; 
char var_architecture[PATH_MAX]; 
char var_packages[PATH_MAX]; 
char var_release[PATH_MAX]; 
char package_location[PATH_MAX];
char package_name[PATH_MAX];
int gameover = 0; 






int fcheckfile(const char *a_option)
{
	char dir1[PATH_MAX]; 
	char *dir2;
	DIR *dip;
	strncpy( dir1 , "",  PATH_MAX  );
	strncpy( dir1 , a_option,  PATH_MAX  );

	struct stat st_buf; 
	int status; 
	int fileordir = 0 ; 

	status = stat ( dir1 , &st_buf);
	if (status != 0) {
		fileordir = 0;
	}
	FILE *fp2check = fopen( dir1  ,"r");
	if( fp2check ) {
		fileordir = 1; 
		fclose(fp2check);
	} 

	if (S_ISDIR (st_buf.st_mode)) {
		fileordir = 2; 
	}
	return fileordir;
}







char *fbasename(char *name)
{
	char *base = name;
	while (*name)
	{
		if (*name++ == '/')
		{
			base = name;
		}
	}
	return (base);
}

char *str_tail( char *name,  int delimiter )
{
	char *base = name;
	while (*name)
	{
		if (*name++ ==  delimiter )
		{
			base = name;
		}
	}
	return (base);
}





char *str_arg1(char *str)
{  
      char ptr[ 5* strlen(str)+1];
      int i,j=0;
      int fspace = 0; 
      for(i=0; str[i]!='\0'; i++)
      {
        if ( str[i] == ' ' ) 
	{
          fspace = 1;
	}
        else
	{
	  if ( fspace == 0 ) 
             ptr[j++]=str[i];
	}
      } 
      ptr[j]='\0';
      size_t siz = 1 + sizeof ptr ; 
      char *r = malloc( 1 +  sizeof ptr );
      return r ? memcpy(r, ptr, siz ) : NULL;
}








void procedure_ls()
{ 
	DIR *dirp;
	struct dirent *dp;
	dirp = opendir( "." );
	while  ((dp = readdir( dirp )) != NULL ) 
	{
		if (  strcmp( dp->d_name, "." ) != 0 )
			if (  strcmp( dp->d_name, ".." ) != 0 )
				printf( "%s\n", dp->d_name );
	}
	closedir( dirp );
}



void procedure_list_variables()
{
        printf( "Repository:   %s\n" , var_repository    ); 
        printf( "Architecture: %s\n" , var_architecture  ); 
        printf( "Release:      %s\n" , var_release       ); 
        printf( "Packages:     %s\n" , var_packages      ); 
}









void procedure_help()
{
        printf( " ==== Guide Help ====     \n"); 
        printf( " slackstrap get slackware     : get ISO for amd64      \n"); 
	printf( " ----  \n" ); 
	printf( "  SETS \n" ); 
	printf( " ----  \n" ); 
	printf( " slackstrap get base-barebone : get the set a very small system (no kernel)\n" ); 
        printf( " slackstrap get kernel        : get the set kernel            \n" ); 
        printf( " slackstrap get kernel-huge   : get the set kernel-huge       \n" ); 
        printf( " slackstrap get base          : get the set base   (no incl. kernel)\n" ); 
	printf( " slackstrap get pkg-base      : get the set pkg-base \n" ); 
	printf( " \n" ); 
	printf( " (Note the minimum Sets to operate the system is kernel + base).\n" ); 
	printf( " \n" ); 
	printf( " ----  \n" ); 
	printf( "  PKG \n" ); 
	printf( " ----  \n" ); 
	printf( " slackstrap links repository  : browse repository from url \n" ); 
	printf( " slackstrap update            : check/update packages.txt database file from repository\n" ); 
	printf( " slackstrap reupdate          : re-update packages.txt (overwrite) database file from repository\n" ); 
	printf( " slackstrap load db           : wget packages.txt database file from repository\n" ); 
	printf( " slackstrap load database     : wget packages.txt database file from repository\n" ); 
	printf( " slackstrap links pkg         : browse locally ./packages.txt file\n" ); 
	printf( " slackstrap list              : list all packages\n" ); 
	printf( " slackstrap find item         : find an item into the locally ./packages.txt file\n" ); 
printf( " slackstrap grep item         : grep and read an item into the locally ./packages.txt file\n" ); 
	printf( " slackstrap fetch item        : find locally ./packages.txt file and wget txz\n" ); 
	printf( " slackstrap --fetch item        : find locally ./packages.txt file and wget txz\n" ); 
	printf( " slackstrap get item        : find locally ./packages.txt file and wget txz\n" ); 
	printf( " slackstrap install item      : fetch and install a package (fetch, prompt, install)\n" ); 
	printf( " slackstrap installpkg item   : fetch and install a package (same, but automatic) \n" ); 
	printf( " ----  \n" ); 
	printf( " slackstrap show sys          : view the variables of current system\n" ); 
	printf( " ----  \n" ); 
	printf( " \n" ); 
	printf( " ----  \n" ); 
	printf( "  BASE \n" ); 
	printf( " ----  \n" ); 
	printf( " slackstrap base              : show strap files for amd64 base strap\n"); 
	printf( " slackstrap kde               : show strap files for amd64 kde  strap\n"); 
	printf( " slackstrap full              : show strap files for amd64 full strap\n"); 
	printf( " ----\n" ); 
	printf( "\n" ); 
	printf( " \n" ); 
	printf( " ----  \n" ); 
	printf( "  INTERPRETER \n" ); 
	printf( " ----  \n" ); 
	printf( "      command:  help \n"); 
	printf( "      command:  ls \n"); 
	printf( "      command:  exit \n"); 
	printf( " ----\n" ); 
	printf( " ----  \n" ); 
	printf( "  PKG \n" ); 
	printf( " ----  \n" ); 
	printf( " slackstrap links repository  : browse repository from url \n" ); 
	printf( " slackstrap update            : update packages.txt database file from repository\n" ); 
	printf( " slackstrap load db           : wget packages.txt database file from repository\n" ); 
	printf( " slackstrap load database     : wget packages.txt database file from repository\n" ); 
	printf( " slackstrap links pkg         : browse locally ./packages.txt file\n" ); 
	printf( " slackstrap list              : list all packages\n" ); 
	printf( " slackstrap find item         : find an item into the locally ./packages.txt file\n" ); 
	printf( "\n" ); 
}






void nsystem( char *mycmd )
{
        char foocharo[PATH_MAX];
	printf( "<CMD: %s>\n", mycmd );
	snprintf( foocharo , sizeof( foocharo ), "%s" , mycmd ); 
	system(   foocharo ); 
	printf( " Defunc CMD: %s>\n", mycmd );
}





void ncmdwith( char *mycmd, char *myfile )
{
	char cmdi[PATH_MAX];
	printf( "** CMD (start) (OS: %d)\n" , MYOS );
        char foocharo[PATH_MAX];
	snprintf( foocharo , sizeof( foocharo ), " %s \"%s\" " , mycmd , myfile ); 
	nsystem(   foocharo ); 
	printf( "** CMD (completed) (OS: %d)\n" , MYOS );
}




void fetch_file_ftp( char *pattern )
{
	char foocwd[PATH_MAX];
        int fetch_file_ftp_force_httpwget = 0; 
        int fetch_file_ftp_force_bsdwget  = 0; 
	printf( "=========================\n" );
	printf( " ===  Fetch File FTP === \n" );
	printf( "=========================\n" );
	printf( "PWD: %s\n", getcwd( foocwd , PATH_MAX ) );
	printf( " ...\n" );

        if ( fcheckfile( "/etc/nconfig_httpwget" ) == 1 ) 
	   fetch_file_ftp_force_httpwget = 1; 
        if ( fcheckfile( "/etc/nconfig_bsdwget" ) == 1 ) 
	   fetch_file_ftp_force_bsdwget = 1; 

	if ( MYOS != 1 )  // BSD
	    ncmdwith( " ftp  " ,  pattern   );
	else if ( MYOS == 1 )  // linux
	    ncmdwith( " wget -c --no-check-certificate  " ,  pattern   );

        /// rescue for ssl handshake
        if ( fetch_file_ftp_force_bsdwget == 1) 
	{
	    printf( " => FORCE\n"); 
	    ncmdwith( "  /usr/pkg/bin/wget -c --no-check-certificate " ,  pattern   );
        }
	else if ( fetch_file_ftp_force_httpwget == 1) 
	{
	    printf( " => FORCE\n"); 
	    ncmdwith( " httpwget  " ,  pattern   );
	}

	printf( " ...\n" );
	printf( "======================\n" );
}
















void fetch_file_ftp_basic( char *pattern )
{
	char foocwd[PATH_MAX];
	printf( "=========================\n" );
	printf( " ===  Fetch File FTP === \n" );
	printf( "=========================\n" );
	printf( "PWD: %s\n", getcwd( foocwd , PATH_MAX ) );

	if ( MYOS != 1 )  // BSD
	    ncmdwith( " ftp  " ,  pattern   );

	else if ( MYOS == 1 )  // linux
	    ncmdwith( " wget -c --no-check-certificate  " ,  pattern   );

	printf( " ...\n" );
	printf( "======================\n" );
}













void grepfile( char *filesource , char *sstring, int foomode )
{
	FILE *source; 
	int c,j ;
	char lline[PATH_MAX]; 
	char mysstring[PATH_MAX]; 
	strncpy( mysstring, sstring,  PATH_MAX); int pcc=0;
	int begin;
	int location_open = 0; 
	j = 0;
	printf( "FILENAME: %s\n" , filesource ); 
	source = fopen( filesource , "r");
	if ( fcheckfile( filesource ) == 1 ) 
	{
		{    c = fgetc(source);
			while( c != EOF )
			{
				if ( c != '\n' ) 
					lline[j++] = c;

				begin = 0;
				if ( c == '\n' )
				{  
					begin = 1;
					lline[j]='\0';

					if ( lline[0] != '#' ) 
                                        if ( location_open == 1 ) 
					{
					       if ( strstr( lline, "PACKAGE" ) != 0 ) 
					       if ( strstr( lline, "LOCATION" ) != 0 ) 
					       {
						     strncpy( package_location , str_tail( lline, ' ' ) , PATH_MAX ); 
					       }
					       location_open = 0; 
					}

					if ( lline[0] != '#' ) 
					if ( strstr( lline, mysstring ) != 0 )
					{
					    if ( foomode == 3 )  // mode silent
					    {
					       if ( strstr( lline, "PACKAGE" ) != 0 ) 
					       if ( strstr( lline, "NAME" ) != 0 ) 
					       {
						     //printf( "%s\n", lline );
						     strncpy( package_name , str_tail( lline, ' ' ) , PATH_MAX ); 
						     printf( "%s\n", package_name );
						     location_open = 1;
					       }

					    }
					    else if ( foomode == 2 )  // mode find package, default 
					    {

					       if ( strstr( lline, "PACKAGE" ) != 0 ) 
					       if ( strstr( lline, "NAME" ) != 0 ) 
					       {
						     printf( "%s\n", lline );
						     strncpy( package_name , str_tail( lline, ' ' ) , PATH_MAX ); 
						     location_open = 1;
					       }

					    }
					    else 
					    {
						printf( "%s\n", lline );
					    }
						//strncpy( autologin_username, fgetusername( lline  ) , 128 ); 
					}

					j = 0;
					lline[j]='\0';
				}
				c = fgetc(source);
			}
			fclose(source);
		}
	}
}








void npkg( char *foocmd1, char *foocmd2 ) 
{
                char charo[PATH_MAX];
		int fookey = 0 ; 
	        //printf( "FIND!\n");
	        printf( "NPKG:\n");
	        printf( "Command: [%s]\n", foocmd1);
	        printf( "Package: [%s]\n", foocmd2);
		if ( fcheckfile( "PACKAGES.TXT" ) == 1 ) 
		{
                   grepfile( "PACKAGES.TXT" , foocmd2 , 2 );  // mode PACKAGE NAME 
		   printf( "PACKAGE NAME:       %s\n" , package_name     ); 
		   printf( "PACKAGE LOCATION:   %s\n" , package_location ); 
		   printf( "PACKAGE URL (MAIN): %s\n" , var_repository ); 
		   snprintf( charo , sizeof( charo ), "%s/%s/%s", var_repository , package_location, package_name ); 
		   printf( "PACKAGE URL:        %s\n" , charo ); 
		   fetch_file_ftp( charo ); 
		   snprintf( charo , sizeof( charo ), "installpkg \"%s\" ", package_name ); 
		   printf( "PACKAGE CMD:        %s\n" , charo ); 

		   if ( fcheckfile( package_name ) == 1 ) 
		   {
		      printf( "FILE FOUND (1)\n" );
		      printf( "PACKAGE TO INSTALL: %s\n", package_name );
	              if ( strcmp( foocmd1 , "install" ) ==  0 ) 
		      {
		         printf( "\n");
		         printf( "<Press 1 or y Key to Install and Continue>\n"); 
		         printf( "\n");
		         fookey = getchar(); 
			 if ( ( fookey == '1' ) || ( fookey == 'y' ) ) 
			   nsystem( charo ); 
		      }
		      else if ( ( strcmp( foocmd1 , "installpkg" ) ==  0 ) 
		        || ( strcmp( foocmd1 , "pkg" ) ==  0 ) )
		      {
		          nsystem( charo ); 
	   	      }
		   }
		}
		strncpy( package_name,     "", PATH_MAX ); 
		strncpy( package_location, "", PATH_MAX ); 
}
















void procedure_run_interpreter( int fooargct , char *foocmd1, char *foocmd2 )
{

        int found = 0; 
	char foocwd[PATH_MAX];
	int fookey = 0; 
	char charo[PATH_MAX];
	int fooargc = fooargct+1; 
	printf( "=========================\n" );
	printf( " ===  Interpreter    === \n" );
	printf( "=========================\n" );



	if ( found == 0 ) 
	if ( foocmd1[0] == '!' )
	{
	        printf( "1.): %s\n" , foocmd1 ); 
	        printf( "2.): %s\n" , foocmd2 ); 
	        printf( "Cut: %s\n" , str_tail( foocmd1 , ' ' ) );
		found=1; 
	}




	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "get" ) ==  0 ) 
	if ( strcmp( foocmd2 , "base-barebone" ) ==  0 ) 
	{
		fetch_file_ftp("https://gitlab.com/openbsd98324/slackware-repository/-/raw/main/slackware/14.2/amd64/usr/sets/base-barebone.tar.gz");
		found=1; 
	}




	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "pwd" ) ==  0 ) 
	{
	        printf( "PWD: %s\n", getcwd( foocwd , PATH_MAX ) );
		found=1; 
	}



	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "hello" ) ==  0 ) 
	{
	        printf( "PWD: %s (hello world)\n", getcwd( foocwd , PATH_MAX ) );
		found=1; 
	}






	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "get" ) ==  0 ) 
	if ( strcmp( foocmd2 , "base" ) ==  0 ) 
	{
	        printf( " => The base has the kernel and modules, and the base system.\n" ); 
		fetch_file_ftp("https://gitlab.com/openbsd98324/slackware-repository/-/raw/main/slackware/14.2/amd64/usr/sets/base.tar.gz");
		found=1; 
	}


	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "links" ) ==  0 ) 
	if ( strcmp( foocmd2 , "repository" ) ==  0 ) 
	{
	        ncmdwith( " links " , var_repository  ); 
		found=1; 
	}


	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "links" ) ==  0 ) 
	if ( strcmp( foocmd2 , "pkg" ) ==  0 ) 
	{
		nsystem( " links PACKAGES.TXT "); 
		found=1; 
	}





	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "load" ) ==  0 ) 
	if ( strcmp( foocmd2 , "database" ) ==  0 ) 
	{
		fetch_file_ftp( var_packages ); 
		found=1; 
	}


	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "load" ) ==  0 ) 
	if ( strcmp( foocmd2 , "db" ) ==  0 ) 
	{
		fetch_file_ftp( var_packages ); 
		found=1; 
	}







	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "update" ) ==  0 ) 
	{
	        ///nsystem("  rm PACKAGES.TXT "); 
		if ( fcheckfile( "PACKAGES.TXT" ) == 1 )  
		{
		   printf( " => FILE PACKAGES.TXT FOUND (OK).\n" ); 
		}
		else
		{
		   printf( "FILE PACKAGES.TXT NOT FOUND.\n" ); 
		   fetch_file_ftp( var_packages ); 
		}
		found=1; 
	}




	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "reupdate" ) ==  0 ) 
	{
	        nsystem("  rm PACKAGES.TXT "); 
		fetch_file_ftp( var_packages ); 
		found=1; 
	}




	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "get" ) ==  0 ) 
	if ( strcmp( foocmd2 , "pkg-base" ) ==  0 ) 
	{
	        printf( " => The base has the kernel and modules, and the base system.\n" ); 
		fetch_file_ftp("https://gitlab.com/openbsd98324/slackware-repository/-/raw/main/slackware/14.2/amd64/usr/sets/pkg-base.tar.gz");
		found=1; 
	}




	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "get" ) ==  0 ) 
	if ( strcmp( foocmd2 , "kernel" ) ==  0 ) 
	{
		fetch_file_ftp("https://gitlab.com/openbsd98324/slackware-repository/-/raw/main/slackware/14.2/amd64/usr/sets/kernel.tar.gz");
		found=1; 
	}









	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "get" ) ==  0 ) 
	if ( strcmp( foocmd2 , "kernel-huge" ) ==  0 ) 
	{
		fetch_file_ftp("https://gitlab.com/openbsd98324/slackware-repository/-/raw/main/slackware/14.2/amd64/usr/sets/kernel-huge.tar.gz");
		fetch_file_ftp( "https://gitlab.com/openbsd98324/ascii/-/raw/main/boot/vmlinuz-4.9.0-11-amd64" ); 
		fetch_file_ftp( "https://gitlab.com/openbsd98324/ascii/-/raw/main/boot/initrd.img-4.9.0-11-amd64" ); 
		fetch_file_ftp( "https://gitlab.com/openbsd98324/ascii/-/raw/main/boot/modules-4.9.0-11-amd64.tar.gz" ); 
		fetch_file_ftp( "https://gitlab.com/openbsd98324/ascii/-/raw/main/boot/modules-4.9.0-11-amd64.tar.gz.md5" ); 
		found=1; 
	}




	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "grep" ) ==  0 ) 
	{
	        printf( "GREP!\n");
		if ( fcheckfile( "PACKAGES.TXT" ) == 1 ) 
                   grepfile( "PACKAGES.TXT" , foocmd2 , 1 );  //classic
		found=1; 
	}







	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( ( strcmp( foocmd1 , "installpkg" ) ==  0 ) 
	|| ( strcmp( foocmd1 ,   "pkg" ) ==  0 )   // like in bsd 
	|| ( strcmp( foocmd1 , "install" ) ==  0 ) )
	{
	        //foocmd2 
	        printf( "Package: %s!\n", foocmd2 );
	        if ( strcmp( foocmd1 , "install" ) ==  0 ) 
		   npkg( "install" , foocmd2 ); 
	        else if ( strcmp( foocmd1 , "installpkg" ) ==  0 ) 
		   npkg( "installpkg" , foocmd2 ); 
	        else if ( strcmp( foocmd1 , "pkg" ) ==  0 ) 
		   npkg( "installpkg" , foocmd2 ); 
	        else 
		   npkg( "installpkg" , foocmd2 ); 
		found=1; 
	}








	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( ( strcmp( foocmd1 , "fetch" ) ==  0 ) 
	|| ( strcmp( foocmd1 ,   "--fetch" ) ==  0 ) 
	|| ( strcmp( foocmd1 ,   "get" ) ==  0 ) )
	{
	        printf( "FIND!\n");
		if ( fcheckfile( "PACKAGES.TXT" ) == 1 ) 
		{
                   grepfile( "PACKAGES.TXT" , foocmd2 , 2 );  // mode PACKAGE NAME 
		   printf(   "PACKAGE NAME:       %s\n" , package_name     ); 
		   printf(   "PACKAGE LOCATION:   %s\n" , package_location ); 
		   printf(   "PACKAGE URL (MAIN): %s\n" , var_repository ); 
		   snprintf( charo , sizeof( charo ), "%s/%s/%s", var_repository , package_location, package_name ); 
		   printf( "PACKAGE URL:        %s\n" , charo ); 
		   fetch_file_ftp( charo ); 
		}
		found=1; 
	}






	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "find" ) ==  0 ) 
	{
	        printf( "FIND!\n");
		if ( fcheckfile( "PACKAGES.TXT" ) == 1 ) 
                   grepfile( "PACKAGES.TXT" , foocmd2 , 2 );  // mode PACKAGE NAME 
		found=1; 
	}


        // not in help
	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "findsilent" ) ==  0 ) 
	{
	        //printf( "FIND!\n");
		if ( fcheckfile( "PACKAGES.TXT" ) == 1 ) 
                   grepfile( "PACKAGES.TXT" , foocmd2 , 3 );  // mode PACKAGE NAME with silent, for grep
		found=1; 
	}



	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "list" ) ==  0 ) 
	{
	        printf( "FIND!\n");
		if ( fcheckfile( "PACKAGES.TXT" ) == 1 ) 
                   grepfile( "PACKAGES.TXT" , "txz" , 2 );  // mode PACKAGE NAME 
		found=1; 
	}





	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "show" ) ==  0 ) 
	if ( strcmp( foocmd2 , "sys" ) ==  0 ) 
	{
	        printf( "SYS!\n");
	        procedure_list_variables();
		found=1; 
	}




	if ( found == 0 ) 
	if ( fooargc == 3 )
	if ( strcmp( foocmd1 , "get" ) ==  0 ) 
	if ( strcmp( foocmd2 , "slackware" ) ==  0 ) 
	{
		fetch_file_ftp( "https://mirrors.slackware.com/slackware/slackware-iso/slackware64-14.2-iso/slackware64-14.2-install-dvd.iso" );
		printf( " => 522db1d2845aaab22078530c67f858c1  slackware64-14.2-install-dvd.iso \n" );
		found=1; 
	}







	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( ( strcmp( foocmd1 , "amd64" ) ==  0 ) || ( strcmp( foocmd1 , "base" ) ==  0 ) )
	{
		printf( "aaa_base-14.2-x86_64-2\n" );
		printf( "aaa_elflibs-14.2-x86_64-23\n" );
		printf( "aaa_terminfo-5.9-x86_64-1\n" );
		printf( "acl-2.2.52-x86_64-1\n" );
		printf( "acpid-2.0.26-x86_64-1\n" );
		printf( "alpine-2.20-x86_64-2\n" );
		printf( "attr-2.4.47-x86_64-1\n" );
		printf( "autofs-5.0.7-x86_64-2\n" );
		printf( "bash-4.3.046-x86_64-1\n" );
		printf( "biff+comsat-0.17-x86_64-1\n" );
		printf( "bin-11.1-x86_64-1\n" );
		printf( "bind-9.10.4_P1-x86_64-1\n" );
		printf( "bluez-5.40-x86_64-1\n" );
		printf( "bluez-firmware-1.2-x86_64-1\n" );
		printf( "bootp-2.4.3-x86_64-2\n" );
		printf( "bridge-utils-1.5-x86_64-1\n" );
		printf( "bsd-finger-0.17-x86_64-1\n" );
		printf( "btrfs-progs-v4.5.3-x86_64-1\n" );
		printf( "bzip2-1.0.6-x86_64-1\n" );
		printf( "ca-certificates-20160104-noarch-1\n" );
		printf( "cifs-utils-6.4-x86_64-2\n" );
		printf( "conntrack-tools-1.4.3-x86_64-1\n" );
		printf( "coreutils-8.25-x86_64-2\n" );
		printf( "cpio-2.12-x86_64-1\n" );
		printf( "cpufrequtils-008-x86_64-1\n" );
		printf( "crda-3.18-x86_64-3\n" );
		printf( "cryptsetup-1.7.1-x86_64-1\n" );
		printf( "curl-7.49.1-x86_64-1\n" );
		printf( "cyrus-sasl-2.1.26-x86_64-1\n" );
		printf( "dbus-1.10.8-x86_64-1\n" );
		printf( "dcron-4.5-x86_64-5\n" );
		printf( "devs-2.3.1-noarch-25\n" );
		printf( "dhcp-4.3.4-x86_64-1\n" );
		printf( "dhcpcd-6.8.2-x86_64-2\n" );
		printf( "dialog-1.2_20130523-x86_64-1\n" );
		printf( "dirmngr-1.1.1-x86_64-3\n" );
		printf( "dnsmasq-2.76-x86_64-1\n" );
		printf( "dosfstools-3.0.28-x86_64-1\n" );
		printf( "e2fsprogs-1.43.1-x86_64-1\n" );
		printf( "ebtables-2.0.10-x86_64-1\n" );
		printf( "ed-1.13-x86_64-1\n" );
		printf( "efibootmgr-0.5.4-x86_64-1\n" );
		printf( "eject-2.1.5-x86_64-4\n" );
		printf( "elilo-3.16-x86_64-2\n" );
		printf( "elm-2.5.8-x86_64-3\n" );
		printf( "elvis-2.2_0-x86_64-2\n" );
		printf( "epic5-2.0-x86_64-1\n" );
		printf( "etc-14.2-x86_64-7\n" );
		printf( "ethtool-4.5-x86_64-1\n" );
		printf( "eudev-3.1.5-x86_64-8\n" );
		printf( "fetchmail-6.3.26-x86_64-2\n" );
		printf( "file-5.25-x86_64-1\n" );
		printf( "findutils-4.4.2-x86_64-1\n" );
		printf( "floppy-5.5-x86_64-1\n" );
		printf( "gawk-4.1.3-x86_64-1\n" );
		printf( "genpower-1.0.5-x86_64-2\n" );
		printf( "getmail-4.47.0-x86_64-1\n" );
		printf( "gettext-0.19.8.1-x86_64-1\n" );
		printf( "getty-ps-2.1.0b-x86_64-2\n" );
		printf( "glibc-solibs-2.23-x86_64-1\n" );
		printf( "glibc-zoneinfo-2016e-noarch-1\n" );
		printf( "gnupg-1.4.20-x86_64-1\n" );
		printf( "gnupg2-2.0.30-x86_64-1\n" );
		printf( "gnutls-3.4.13-x86_64-1\n" );
		printf( "gpa-0.9.9-x86_64-1\n" );
		printf( "gpgme-1.6.0-x86_64-1\n" );
		printf( "gpm-1.20.7-x86_64-3\n" );
		printf( "gptfdisk-1.0.0-x86_64-1\n" );
		printf( "grep-2.25-x86_64-1\n" );
		printf( "grub-2.00-x86_64-5\n" );
		printf( "gzip-1.8-x86_64-1\n" );
		printf( "hdparm-9.48-x86_64-1\n" );
		printf( "htdig-3.2.0b6-x86_64-4\n" );
		printf( "httpd-2.4.20-x86_64-1\n" );
		printf( "hwdata-0.284-noarch-1\n" );
		printf( "icmpinfo-1.11-x86_64-2\n" );
		printf( "idnkit-1.0-x86_64-1\n" );
		printf( "iftop-1.0pre2-x86_64-1\n" );
		printf( "imapd-2.20-x86_64-2\n" );
		printf( "inetd-1.79s-x86_64-9\n" );
		printf( "infozip-6.0-x86_64-3\n" );
		printf( "inotify-tools-3.14-x86_64-1\n" );
		printf( "iproute2-4.4.0-x86_64-1\n" );
		printf( "ipset-6.20-x86_64-1\n" );
		printf( "iptables-1.6.0-x86_64-2\n" );
		printf( "iptraf-ng-1.1.4-x86_64-1\n" );
		printf( "iputils-s20140519-x86_64-1\n" );
		printf( "ipw2100-fw-1.3-fw-1\n" );
		printf( "ipw2200-fw-3.1-fw-1\n" );
		printf( "irssi-0.8.19-x86_64-1\n" );
		printf( "iw-4.3-x86_64-1\n" );
		printf( "jfsutils-1.1.15-x86_64-1\n" );
		printf( "kbd-1.15.3-x86_64-2\n" );
		printf( "kernel-firmware-20160628git-noarch-1\n" );
		printf( "kernel-generic-4.4.14-x86_64-1\n" );
		printf( "kernel-huge-4.4.14-x86_64-1\n" );
		printf( "kernel-modules-4.4.14-x86_64-1\n" );
		printf( "kmod-22-x86_64-1\n" );
		printf( "less-481-x86_64-1\n" );
		printf( "lftp-4.7.2-x86_64-1\n" );
		printf( "lha-114i-x86_64-1\n" );
		printf( "libassuan-2.4.2-x86_64-1\n" );
		printf( "libcgroup-0.41-x86_64-1\n" );
		printf( "libgcrypt-1.7.1-x86_64-1\n" );
		printf( "libgpg-error-1.23-x86_64-1\n" );
		printf( "libgudev-230-x86_64-1\n" );
		printf( "libksba-1.3.3-x86_64-1\n" );
		printf( "libmbim-1.12.2-x86_64-2\n" );
		printf( "libmnl-1.0.3-x86_64-1\n" );
		printf( "libndp-1.6-x86_64-1\n" );
		printf( "libnetfilter_acct-1.0.2-x86_64-1\n" );
		printf( "libnetfilter_conntrack-1.0.5-x86_64-1\n" );
		printf( "libnetfilter_cthelper-1.0.0-x86_64-1\n" );
		printf( "libnetfilter_cttimeout-1.0.0-x86_64-1\n" );
		printf( "libnetfilter_log-1.0.1-x86_64-1\n" );
		printf( "libnetfilter_queue-1.0.2-x86_64-1\n" );
		printf( "libnfnetlink-1.0.1-x86_64-1\n" );
		printf( "libnftnl-1.0.6-x86_64-1\n" );
		printf( "libqmi-1.12.6-x86_64-1\n" );
		printf( "libtirpc-1.0.1-x86_64-2\n" );
		printf( "lilo-24.2-x86_64-2\n" );
		printf( "links-2.12-x86_64-2\n" );
		printf( "list.md5\n" );
		printf( "logrotate-3.8.9-x86_64-1\n" );
		printf( "lrzip-0.621-x86_64-1\n" );
		printf( "lvm2-2.02.154-x86_64-1\n" );
		printf( "lynx-2.8.8rel.2-x86_64-1\n" );
		printf( "mailx-12.5-x86_64-2\n" );
		printf( "mcabber-1.0.1-x86_64-1\n" );
		printf( "mcelog-128-x86_64-1\n" );
		printf( "mdadm-3.3.4-x86_64-1\n" );
		printf( "metamail-2.7-x86_64-5\n" );
		printf( "minicom-2.6.2-x86_64-1\n" );
		printf( "mkinitrd-1.4.8-x86_64-8\n" );
		printf( "mobile-broadband-provider-info-20151214-x86_64-1\n" );
		printf( "mt-st-0.9b-x86_64-2\n" );
		printf( "mtr-0.86-x86_64-1\n" );
		printf( "mtx-1.3.12-x86_64-1\n" );
		printf( "mutt-1.6.1-x86_64-1\n" );
		printf( "nc-1.10-x86_64-1\n" );
		printf( "ncftp-3.2.5-x86_64-1\n" );
		printf( "ncompress-4.2.4.4-x86_64-1\n" );
		printf( "net-snmp-5.7.3-x86_64-3\n" );
		printf( "net-tools-1.60.20120726git-x86_64-1\n" );
		printf( "netatalk-2.2.3-x86_64-6\n" );
		printf( "netdate-bsd4-x86_64-1\n" );
		printf( "netkit-bootparamd-0.17-x86_64-2\n" );
		printf( "netkit-ftp-0.17-x86_64-2\n" );
		printf( "netkit-ntalk-0.17-x86_64-3\n" );
		printf( "netkit-routed-0.17-x86_64-1\n" );
		printf( "netkit-rsh-0.17-x86_64-1\n" );
		printf( "netkit-rusers-0.17-x86_64-1\n" );
		printf( "netkit-rwall-0.17-x86_64-1\n" );
		printf( "netkit-rwho-0.17-x86_64-2\n" );
		printf( "netkit-timed-0.17-x86_64-1\n" );
		printf( "netpipes-4.2-x86_64-1\n" );
		printf( "nettle-3.2-x86_64-1\n" );
		printf( "netwatch-1.3.1_2-x86_64-1\n" );
		printf( "network-scripts-14.2-noarch-1\n" );
		printf( "netwrite-0.17-x86_64-1\n" );
		printf( "newspost-2.1.1-x86_64-1\n" );
		printf( "nfacct-1.0.1-x86_64-1\n" );
		printf( "nfs-utils-1.3.3-x86_64-2\n" );
		printf( "nftables-0.6-x86_64-1\n" );
		printf( "nmap-7.12-x86_64-1\n" );
		printf( "nn-6.7.3-x86_64-3\n" );
		printf( "ntfs-3g-2016.2.22-x86_64-1\n" );
		printf( "ntp-4.2.8p8-x86_64-1\n" );
		printf( "obexftp-0.24-x86_64-1\n" );
		printf( "openldap-client-2.4.42-x86_64-1\n" );
		printf( "openobex-1.7.1-x86_64-1\n" );
		printf( "openssh-7.2p2-x86_64-1\n" );
		printf( "openssl-1.0.2h-x86_64-1\n" );
		printf( "openssl-solibs-1.0.2h-x86_64-1\n" );
		printf( "openvpn-2.3.11-x86_64-1\n" );
		printf( "os-prober-1.70-x86_64-1\n" );
		printf( "p11-kit-0.23.2-x86_64-1\n" );
		printf( "packages.lst\n" );
		printf( "patch-2.7.5-x86_64-1\n" );
		printf( "pciutils-3.4.1-x86_64-2\n" );
		printf( "pcmciautils-018-x86_64-1\n" );
		printf( "php-5.6.23-x86_64-1\n" );
		printf( "pidentd-3.0.19-x86_64-2\n" );
		printf( "pinentry-0.9.7-x86_64-1\n" );
		printf( "pkgtools-14.2-noarch-10\n" );
		printf( "popa3d-1.0.3-x86_64-1\n" );
		printf( "ppp-2.4.7-x86_64-1\n" );
		printf( "procmail-3.22-x86_64-2\n" );
		printf( "procps-ng-3.3.11-x86_64-1\n" );
		printf( "proftpd-1.3.5b-x86_64-1\n" );
		printf( "pssh-2.3.1-x86_64-1\n" );
		printf( "pth-2.0.7-x86_64-1\n" );
		printf( "quota-4.03-x86_64-1\n" );
		printf( "rdist-6.1.5-x86_64-2\n" );
		printf( "reiserfsprogs-3.6.24-x86_64-1\n" );
		printf( "rfkill-0.5-x86_64-1\n" );
		printf( "rp-pppoe-3.12-x86_64-1\n" );
		printf( "rpcbind-0.2.3-x86_64-1\n" );
		printf( "rpm2tgz-1.2.2-x86_64-1\n" );
		printf( "rsync-3.1.2-x86_64-1\n" );
		printf( "samba-4.4.4-x86_64-3\n" );
		printf( "sdparm-1.10-x86_64-1\n" );
		printf( "sed-4.2.2-x86_64-1\n" );
		printf( "sendmail-8.15.2-x86_64-2\n" );
		printf( "sendmail-cf-8.15.2-noarch-2\n" );
		printf( "shadow-4.2.1-x86_64-1\n" );
		printf( "sharutils-4.15.2-x86_64-1\n" );
		printf( "slocate-3.1-x86_64-4\n" );
		printf( "slrn-1.0.2-x86_64-1\n" );
		printf( "smartmontools-6.5-x86_64-1\n" );
		printf( "snownews-1.5.12-x86_64-2\n" );
		printf( "splitvt-1.6.5-x86_64-1\n" );
		printf( "stunnel-5.31-x86_64-1\n" );
		printf( "sysfsutils-2.1.0-x86_64-1\n" );
		printf( "sysklogd-1.5.1-x86_64-2\n" );
		printf( "syslinux-4.07-x86_64-1\n" );
		printf( "sysvinit-2.88dsf-x86_64-4\n" );
		printf( "sysvinit-functions-8.53-x86_64-2\n" );
		printf( "sysvinit-scripts-2.0-noarch-33\n" );
		printf( "tar-1.29-x86_64-1\n" );
		printf( "tcp_wrappers-7.6-x86_64-1\n" );
		printf( "tcpdump-4.7.4-x86_64-1\n" );
		printf( "tcsh-6.19.00-x86_64-1\n" );
		printf( "telnet-0.17-x86_64-2\n" );
		printf( "tftp-hpa-5.2-x86_64-2\n" );
		printf( "time-1.7-x86_64-1\n" );
		printf( "tin-2.2.1-x86_64-3\n" );
		printf( "traceroute-2.0.21-x86_64-1\n" );
		printf( "tree-1.7.0-x86_64-1\n" );
		printf( "trn-3.6-x86_64-2\n" );
		printf( "udisks-1.0.5-x86_64-3\n" );
		printf( "udisks2-2.1.5-x86_64-2\n" );
		printf( "ulogd-2.0.5-x86_64-1\n" );
		printf( "unarj-265-x86_64-1\n" );
		printf( "upower-0.9.23-x86_64-3\n" );
		printf( "usb_modeswitch-2.2.6-x86_64-1\n" );
		printf( "usbutils-008-x86_64-1\n" );
		printf( "utempter-1.1.6-x86_64-2\n" );
		printf( "util-linux-2.27.1-x86_64-1\n" );
		printf( "uucp-1.07-x86_64-1\n" );
		printf( "vlan-1.9-x86_64-2\n" );
		printf( "vsftpd-3.0.3-x86_64-3\n" );
		printf( "wget-1.18-x86_64-1\n" );
		printf( "which-2.21-x86_64-1\n" );
		printf( "whois-5.2.12-x86_64-1\n" );
		printf( "wireless-tools-29-x86_64-9\n" );
		printf( "wpa_supplicant-2.5-x86_64-1\n" );
		printf( "xfsprogs-4.3.0-x86_64-1\n" );
		printf( "xz-5.2.2-x86_64-1\n" );
		printf( "yptools-2.14-x86_64-7\n" );
		printf( "ytalk-3.3.0-x86_64-2\n" );
		printf( "zd1211-firmware-1.4-fw-1\n" );
		printf( "zoo-2.10_22-x86_64-1\n" );

		printf( "ModemManager-1.4.14-x86_64-1\n" );
		printf( "NetworkManager-1.2.2-x86_64-2\n" );
		found=1; 
	}




	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "ls" ) ==  0 ) 
	{
	        printf( "LS!\n");
	        procedure_ls();
		found=1; 
	}


	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "help" ) ==  0 ) 
	{
	        printf( "HELP!\n");
	        procedure_help();
		found=1; 
	}


	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "exit" ) ==  0 ) 
	{
	        printf( "EXIT!\n");
	        gameover = 1; 
		found=1; 
	}



	if ( found == 0 ) 
	if ( fooargc == 2 )
	if ( strcmp( foocmd1 , "kde" ) ==  0 ) 
	{
	        printf( "KDE!\n");
		printf( "ConsoleKit2-1.0.0-x86_64-3\n" );
		printf( "Cython-0.23.4-x86_64-1\n" );
		printf( "GConf-3.2.6-x86_64-3\n" );
		printf( "LibRaw-0.17.2-x86_64-1\n" );
		printf( "M2Crypto-0.23.0-x86_64-1\n" );
		printf( "MPlayer-1.2_20160125-i586-3\n" );
		printf( "MPlayer-1.2_20160125-x86_64-3\n" );
		printf( "ModemManager-1.4.14-x86_64-1\n" );
		printf( "NetworkManager-1.2.2-x86_64-2\n" );
		printf( "PyQt-4.11.4-x86_64-1\n" );
		printf( "QScintilla-2.9.1-x86_64-1\n" );
		printf( "a2ps-4.14-x86_64-5\n" );
		printf( "a52dec-0.7.4-x86_64-2\n" );
		printf( "aaa_base-14.2-x86_64-2\n" );
		printf( "aaa_elflibs-14.2-x86_64-23\n" );
		printf( "aaa_terminfo-5.9-x86_64-1\n" );
		printf( "aalib-1.4rc5-x86_64-5\n" );
		printf( "acct-6.5.4-x86_64-2\n" );
		printf( "acl-2.2.52-x86_64-1\n" );
		printf( "acpid-2.0.26-x86_64-1\n" );
		printf( "adwaita-icon-theme-3.18.0-noarch-1\n" );
		printf( "akonadi-1.13.0-x86_64-2\n" );
		printf( "alpine-2.20-x86_64-2\n" );
		printf( "alsa-lib-1.1.1-x86_64-2\n" );
		printf( "alsa-oss-1.0.28-x86_64-1\n" );
		printf( "alsa-plugins-1.1.1-x86_64-1\n" );
		printf( "alsa-utils-1.1.1-x86_64-1\n" );
		printf( "amarok-2.8.0-x86_64-3\n" );
		printf( "amor-4.14.3-x86_64-2\n" );
		printf( "amp-0.7.6-x86_64-1\n" );
		printf( "analitza-4.14.3-x86_64-2\n" );
		printf( "anthy-9100h-x86_64-1\n" );
		printf( "appres-1.0.4-x86_64-2\n" );
		printf( "apr-1.5.2-x86_64-1\n" );
		printf( "apr-util-1.5.4-x86_64-2\n" );
		printf( "ark-4.14.3-x86_64-2\n" );
		printf( "artikulate-4.14.3-x86_64-2\n" );
		printf( "ash-0.4.0-x86_64-2\n" );
		printf( "aspell-0.60.6.1-x86_64-1\n" );
		printf( "aspell-en-7.1_0-x86_64-1\n" );
		printf( "at-3.1.19-x86_64-2\n" );
		printf( "at-spi2-atk-2.18.1-x86_64-1\n" );
		printf( "at-spi2-core-2.18.3-x86_64-1\n" );
		printf( "atk-2.18.0-x86_64-1\n" );
		printf( "atkmm-2.24.2-x86_64-1\n" );
		printf( "attica-0.4.2-x86_64-1\n" );
		printf( "attr-2.4.47-x86_64-1\n" );
		printf( "audacious-3.7.2-x86_64-1\n" );
		printf( "audacious-plugins-3.7.2-x86_64-1\n" );
		printf( "audiocd-kio-4.14.3-x86_64-2\n" );
		printf( "audiofile-0.3.6-x86_64-1\n" );
		printf( "autoconf-2.69-noarch-1\n" );
		printf( "autofs-5.0.7-x86_64-2\n" );
		printf( "automake-1.15-noarch-1\n" );
		printf( "automoc4-0.9.88-x86_64-1\n" );
		printf( "babl-0.1.14-x86_64-1\n" );
		printf( "baloo-4.14.3-x86_64-2\n" );
		printf( "baloo-widgets-4.14.3-x86_64-2\n" );
		printf( "bash-4.3.046-x86_64-1\n" );
		printf( "bc-1.06.95-x86_64-3\n" );
		printf( "bdftopcf-1.0.5-x86_64-2\n" );
		printf( "beforelight-1.0.5-x86_64-2\n" );
		printf( "biff+comsat-0.17-x86_64-1\n" );
		printf( "bigreqsproto-1.1.2-noarch-1\n" );
		printf( "bin-11.1-x86_64-1\n" );
		printf( "bind-9.10.4_P1-x86_64-1\n" );
		printf( "binutils-2.26-x86_64-3\n" );
		printf( "bison-3.0.4-x86_64-1\n" );
		printf( "bitmap-1.0.8-x86_64-2\n" );
		printf( "blackbox-0.70.1-x86_64-7\n" );
		printf( "blinken-4.14.3-x86_64-2\n" );
		printf( "bluedevil-2.1.1-x86_64-3\n" );
		printf( "blueman-2.0.4-x86_64-1\n" );
		printf( "bluez-5.40-x86_64-1\n" );
		printf( "bluez-firmware-1.2-x86_64-1\n" );
		printf( "bomber-4.14.3-x86_64-2\n" );
		printf( "boost-1.59.0-x86_64-1\n" );
		printf( "bootp-2.4.3-x86_64-2\n" );
		printf( "bovo-4.14.3-x86_64-2\n" );
		printf( "bpe-2.01.00-x86_64-2\n" );
		printf( "bridge-utils-1.5-x86_64-1\n" );
		printf( "bsd-finger-0.17-x86_64-1\n" );
		printf( "bsd-games-2.13-x86_64-12\n" );
		printf( "btrfs-progs-v4.5.3-x86_64-1\n" );
		printf( "bzip2-1.0.6-x86_64-1\n" );
		printf( "ca-certificates-20160104-noarch-1\n" );
		printf( "cairo-1.14.6-x86_64-2\n" );
		printf( "cairomm-1.12.0-x86_64-1\n" );
		printf( "calligra-2.9.11-x86_64-5\n" );
		printf( "cantor-4.14.3-x86_64-2\n" );
		printf( "ccache-3.2.4-x86_64-1\n" );
		printf( "cdparanoia-III_10.2-x86_64-1\n" );
		printf( "cdrdao-1.2.3-x86_64-2\n" );
		printf( "cdrtools-3.01-x86_64-3\n" );
		printf( "cervisia-4.14.3-x86_64-2\n" );
		printf( "cgmanager-0.39-x86_64-1\n" );
		printf( "chmlib-0.40-x86_64-2\n" );
		printf( "cifs-utils-6.4-x86_64-2\n" );
		printf( "clisp-2.49-x86_64-3\n" );
		printf( "clucene-2.3.3.4-x86_64-2\n" );
		printf( "cmake-3.5.2-x86_64-1\n" );
		printf( "compiz-0.8.8-x86_64-4\n" );
		printf( "compositeproto-0.4.2-noarch-1\n" );
		printf( "conntrack-tools-1.4.3-x86_64-1\n" );
		printf( "coreutils-8.25-x86_64-2\n" );
		printf( "cpio-2.12-x86_64-1\n" );
		printf( "cpufrequtils-008-x86_64-1\n" );
		printf( "crda-3.18-x86_64-3\n" );
		printf( "cryptsetup-1.7.1-x86_64-1\n" );
		printf( "cscope-15.8b-x86_64-1\n" );
		printf( "cups-2.1.4-x86_64-1\n" );
		printf( "cups-filters-1.9.0-x86_64-2\n" );
		printf( "curl-7.49.1-x86_64-1\n" );
		printf( "cvs-1.11.23-x86_64-2\n" );
		printf( "cyrus-sasl-2.1.26-x86_64-1\n" );
		printf( "damageproto-1.2.1-noarch-1\n" );
		printf( "db42-4.2.52-x86_64-3\n" );
		printf( "db44-4.4.20-x86_64-3\n" );
		printf( "db48-4.8.30-x86_64-2\n" );
		printf( "dbus-1.10.8-x86_64-1\n" );
		printf( "dbus-glib-0.106-x86_64-1\n" );
		printf( "dbus-python-1.2.4-x86_64-1\n" );
		printf( "dc3dd-7.2.641-x86_64-1\n" );
		printf( "dconf-0.24.0-x86_64-1\n" );
		printf( "dconf-editor-3.18.2-x86_64-1\n" );
		printf( "dcron-4.5-x86_64-5\n" );
		printf( "ddd-3.3.12-x86_64-4\n" );
		printf( "ddrescue-1.21-x86_64-1\n" );
		printf( "dejavu-fonts-ttf-2.34-noarch-1\n" );
		printf( "desktop-file-utils-0.22-x86_64-1\n" );
		printf( "dev86-0.16.21-x86_64-1\n" );
		printf( "devs-2.3.1-noarch-25\n" );
		printf( "dhcp-4.3.4-x86_64-1\n" );
		printf( "dhcpcd-6.8.2-x86_64-2\n" );
		printf( "dialog-1.2_20130523-x86_64-1\n" );
		printf( "diffstat-1.60-x86_64-1\n" );
		printf( "diffutils-3.3-x86_64-1\n" );
		printf( "dirmngr-1.1.1-x86_64-3\n" );
		printf( "distcc-3.1-x86_64-1\n" );
		printf( "djvulibre-3.5.27-x86_64-1\n" );
		printf( "dmapi-2.2.12-x86_64-1\n" );
		printf( "dmidecode-3.0-x86_64-1\n" );
		printf( "dmxproto-2.3.1-noarch-1\n" );
		printf( "dnsmasq-2.76-x86_64-1\n" );
		printf( "dolphin-plugins-4.14.3-x86_64-2\n" );
		printf( "dosfstools-3.0.28-x86_64-1\n" );
		printf( "doxygen-1.8.9.1-x86_64-1\n" );
		printf( "dragon-4.14.3-x86_64-2\n" );
		printf( "dri2proto-2.8-x86_64-2\n" );
		printf( "dri3proto-1.0-x86_64-2\n" );
		printf( "dvd+rw-tools-7.1-x86_64-2\n" );
		printf( "e2fsprogs-1.43.1-x86_64-1\n" );
		printf( "ebook-tools-0.2.2-x86_64-3\n" );
		printf( "ebtables-2.0.10-x86_64-1\n" );
		printf( "ed-1.13-x86_64-1\n" );
		printf( "editres-1.0.6-x86_64-2\n" );
		printf( "efibootmgr-0.5.4-x86_64-1\n" );
		printf( "eigen2-2.0.17-x86_64-1\n" );
		printf( "eigen3-3.2.7-x86_64-2\n" );
		printf( "eject-2.1.5-x86_64-4\n" );
		printf( "electricsheep-20090306-x86_64-4\n" );
		printf( "elfutils-0.163-x86_64-1\n" );
		printf( "elilo-3.16-x86_64-2\n" );
		printf( "elm-2.5.8-x86_64-3\n" );
		printf( "elvis-2.2_0-x86_64-2\n" );
		printf( "enchant-1.6.0-x86_64-1\n" );
		printf( "encodings-1.0.4-noarch-1\n" );
		printf( "enscript-1.6.6-x86_64-1\n" );
		printf( "epic5-2.0-x86_64-1\n" );
		printf( "esound-0.2.41-x86_64-2\n" );
		printf( "etc-14.2-x86_64-7\n" );
		printf( "ethtool-4.5-x86_64-1\n" );
		printf( "eudev-3.1.5-x86_64-8\n" );
		printf( "evieext-1.1.1-noarch-1\n" );
		printf( "exiv2-0.25-x86_64-1\n" );
		printf( "expat-2.1.0-x86_64-1\n" );
		printf( "expect-5.45-x86_64-1\n" );
		printf( "fetchmail-6.3.26-x86_64-2\n" );
		printf( "fftw-3.3.4-x86_64-1\n" );
		printf( "file-5.25-x86_64-1\n" );
		printf( "filelight-4.14.3-x86_64-2\n" );
		printf( "findutils-4.4.2-x86_64-1\n" );
		printf( "fixesproto-5.0-x86_64-2\n" );
		printf( "flac-1.3.1-x86_64-1\n" );
		printf( "flex-2.6.0-x86_64-1\n" );
		printf( "floppy-5.5-x86_64-1\n" );
		printf( "fluxbox-1.3.7-x86_64-1\n" );
		printf( "font-adobe-100dpi-1.0.3-noarch-1\n" );
		printf( "font-adobe-75dpi-1.0.3-noarch-1\n" );
		printf( "font-adobe-utopia-100dpi-1.0.4-noarch-1\n" );
		printf( "font-adobe-utopia-75dpi-1.0.4-noarch-1\n" );
		printf( "font-adobe-utopia-type1-1.0.4-noarch-1\n" );
		printf( "font-alias-1.0.3-noarch-1\n" );
		printf( "font-arabic-misc-1.0.3-noarch-1\n" );
		printf( "font-bh-100dpi-1.0.3-noarch-1\n" );
		printf( "font-bh-75dpi-1.0.3-noarch-1\n" );
		printf( "font-bh-lucidatypewriter-100dpi-1.0.3-noarch-1\n" );
		printf( "font-bh-lucidatypewriter-75dpi-1.0.3-noarch-1\n" );
		printf( "font-bh-ttf-1.0.3-noarch-1\n" );
		printf( "font-bh-type1-1.0.3-noarch-1\n" );
		printf( "font-bitstream-100dpi-1.0.3-noarch-1\n" );
		printf( "font-bitstream-75dpi-1.0.3-noarch-1\n" );
		printf( "font-bitstream-speedo-1.0.2-noarch-1\n" );
		printf( "font-bitstream-type1-1.0.3-noarch-1\n" );
		printf( "font-cronyx-cyrillic-1.0.3-noarch-1\n" );
		printf( "font-cursor-misc-1.0.3-noarch-1\n" );
		printf( "font-daewoo-misc-1.0.3-noarch-1\n" );
		printf( "font-dec-misc-1.0.3-noarch-1\n" );
		printf( "font-ibm-type1-1.0.3-noarch-1\n" );
		printf( "font-isas-misc-1.0.3-noarch-1\n" );
		printf( "font-jis-misc-1.0.3-noarch-1\n" );
		printf( "font-micro-misc-1.0.3-noarch-1\n" );
		printf( "font-misc-cyrillic-1.0.3-noarch-1\n" );
		printf( "font-misc-ethiopic-1.0.3-noarch-1\n" );
		printf( "font-misc-meltho-1.0.3-noarch-1\n" );
		printf( "font-misc-misc-1.1.2-noarch-1\n" );
		printf( "font-mutt-misc-1.0.3-noarch-1\n" );
		printf( "font-schumacher-misc-1.1.2-noarch-1\n" );
		printf( "font-screen-cyrillic-1.0.4-noarch-1\n" );
		printf( "font-sony-misc-1.0.3-noarch-1\n" );
		printf( "font-sun-misc-1.0.3-noarch-1\n" );
		printf( "font-util-1.3.1-x86_64-2\n" );
		printf( "font-winitzki-cyrillic-1.0.3-noarch-1\n" );
		printf( "font-xfree86-type1-1.0.4-noarch-1\n" );
		printf( "fontcacheproto-0.1.3-noarch-1\n" );
		printf( "fontconfig-2.11.1-x86_64-2\n" );
		printf( "fontsproto-2.1.3-noarch-1\n" );
		printf( "fonttosfnt-1.0.4-x86_64-2\n" );
		printf( "freeglut-2.8.1-x86_64-1\n" );
		printf( "freetype-2.6.3-x86_64-1\n" );
		printf( "fribidi-0.19.7-x86_64-1\n" );
		printf( "fslsfonts-1.0.5-x86_64-2\n" );
		printf( "fstobdf-1.0.6-x86_64-2\n" );
		printf( "fuse-2.9.5-x86_64-1\n" );
		printf( "fvwm-2.6.6-x86_64-1\n" );
		printf( "gamin-0.1.10-x86_64-5\n" );
		printf( "gawk-4.1.3-x86_64-1\n" );
		printf( "gc-7.4.2-x86_64-3\n" );
		printf( "gcc-5.3.0-i586-3\n" );
		printf( "gcc-5.3.0-x86_64-3\n" );
		printf( "gcc-g++-5.3.0-x86_64-3\n" );
		printf( "gcc-gfortran-5.3.0-x86_64-3\n" );
		printf( "gcc-gnat-5.3.0-x86_64-3\n" );
		printf( "gcc-go-5.3.0-x86_64-3\n" );
		printf( "gcc-java-5.3.0-x86_64-3\n" );
		printf( "gcc-objc-5.3.0-x86_64-3\n" );
		printf( "gccmakedep-1.0.3-noarch-1\n" );
		printf( "gcr-3.16.0-x86_64-2\n" );
		printf( "gd-2.2.1-x86_64-1\n" );
		printf( "gdb-7.11.1-x86_64-1\n" );
		printf( "gdbm-1.12-x86_64-1\n" );
		printf( "gdk-pixbuf2-2.32.3-x86_64-1\n" );
		printf( "geeqie-1.3-x86_64-1\n" );
		printf( "gegl-0.2.0-x86_64-3\n" );
		printf( "genpower-1.0.5-x86_64-2\n" );
		printf( "getmail-4.47.0-x86_64-1\n" );
		printf( "gettext-0.19.8.1-x86_64-1\n" );
		printf( "gettext-tools-0.19.8.1-x86_64-1\n" );
		printf( "getty-ps-2.1.0b-x86_64-2\n" );
		printf( "gftp-2.0.19-x86_64-4\n" );
		printf( "ghostscript-9.19-x86_64-2\n" );
		printf( "ghostscript-fonts-std-8.11-noarch-1\n" );
		printf( "giflib-5.1.1-x86_64-1\n" );
		printf( "gimp-2.8.16-x86_64-1\n" );
		printf( "git-2.9.0-x86_64-1\n" );
		printf( "gkrellm-2.3.7-x86_64-1\n" );
		printf( "glade3-3.8.5-x86_64-2\n" );
		printf( "glew-1.13.0-x86_64-1\n" );
		printf( "glib-1.2.10-x86_64-3\n" );
		printf( "glib-networking-2.46.1-x86_64-1\n" );
		printf( "glib2-2.46.2-x86_64-2\n" );
		printf( "glibc-2.23-x86_64-1\n" );
		printf( "glibc-i18n-2.23-x86_64-1\n" );
		printf( "glibc-profile-2.23-x86_64-1\n" );
		printf( "glibc-solibs-2.23-x86_64-1\n" );
		printf( "glibc-zoneinfo-2016e-noarch-1\n" );
		printf( "glibmm-2.46.4-x86_64-1\n" );
		printf( "glproto-1.4.17-noarch-1\n" );
		printf( "glu-9.0.0-x86_64-1\n" );
		printf( "gmime-2.6.20-x86_64-1\n" );
		printf( "gmm-4.2-noarch-1\n" );
		printf( "gmp-6.1.1-x86_64-1\n" );
		printf( "gnome-keyring-3.16.0-x86_64-2\n" );
		printf( "gnome-themes-standard-3.18.0-x86_64-1\n" );
		printf( "gnu-cobol-1.1-x86_64-1\n" );
		printf( "gnu-efi-3.0.4-x86_64-1\n" );
		printf( "gnuchess-6.2.2-x86_64-1\n" );
		printf( "gnupg-1.4.20-x86_64-1\n" );
		printf( "gnupg2-2.0.30-x86_64-1\n" );
		printf( "gnuplot-5.0.3-x86_64-2\n" );
		printf( "gnutls-3.4.13-x86_64-1\n" );
		printf( "gobject-introspection-1.46.0-x86_64-1\n" );
		printf( "gpa-0.9.9-x86_64-1\n" );
		printf( "gparted-0.26.1-x86_64-1\n" );
		printf( "gperf-3.0.4-x86_64-1\n" );
		printf( "gpgme-1.6.0-x86_64-1\n" );
		printf( "gphoto2-2.5.10-x86_64-1\n" );
		printf( "gpm-1.20.7-x86_64-3\n" );
		printf( "gptfdisk-1.0.0-x86_64-1\n" );
		printf( "granatier-4.14.3-x86_64-2\n" );
		printf( "grantlee-0.5.1-x86_64-1\n" );
		printf( "grep-2.25-x86_64-1\n" );
		printf( "groff-1.22.3-x86_64-2\n" );
		printf( "grub-2.00-x86_64-5\n" );
		printf( "gsettings-desktop-schemas-3.18.1-x86_64-1\n" );
		printf( "gsl-2.1-x86_64-1\n" );
		printf( "gst-plugins-base-1.6.4-x86_64-1\n" );
		printf( "gst-plugins-base0-0.10.36-x86_64-2\n" );
		printf( "gst-plugins-good-1.6.4-x86_64-1\n" );
		printf( "gst-plugins-good0-0.10.31-x86_64-2\n" );
		printf( "gstreamer-1.6.4-x86_64-1\n" );
		printf( "gstreamer0-0.10.36-x86_64-1\n" );
		printf( "gtk+-1.2.10-x86_64-5\n" );
		printf( "gtk+2-2.24.30-x86_64-1\n" );
		printf( "gtk+3-3.18.9-x86_64-1\n" );
		printf( "gtkmm2-2.24.4-x86_64-1\n" );
		printf( "gtkmm3-3.18.1-x86_64-1\n" );
		printf( "gtkspell-2.0.16-x86_64-3\n" );
		printf( "gucharmap-3.12.1-x86_64-2\n" );
		printf( "guile-2.0.11-x86_64-2\n" );
		printf( "gutenprint-5.2.11-x86_64-2\n" );
		printf( "gv-3.7.4-x86_64-1\n" );
		printf( "gvfs-1.26.3-x86_64-1\n" );
		printf( "gwenview-4.14.3-x86_64-2\n" );
		printf( "gzip-1.8-x86_64-1\n" );
		printf( "harfbuzz-1.2.7-x86_64-1\n" );
		printf( "hdparm-9.48-x86_64-1\n" );
		printf( "help2man-1.46.5-x86_64-1\n" );
		printf( "herqq-1.0.0-x86_64-1\n" );
		printf( "hexchat-2.12.1-x86_64-1\n" );
		printf( "hfsutils-3.2.6-x86_64-5\n" );
		printf( "hicolor-icon-theme-0.15-noarch-1\n" );
		printf( "hplip-3.16.5-x86_64-3\n" );
		printf( "htdig-3.2.0b6-x86_64-4\n" );
		printf( "htop-2.0.1-x86_64-1\n" );
		printf( "httpd-2.4.20-x86_64-1\n" );
		printf( "hunspell-1.3.3-x86_64-1\n" );
		printf( "hwdata-0.284-noarch-1\n" );
		printf( "iceauth-1.0.7-x86_64-2\n" );
		printf( "icmpinfo-1.11-x86_64-2\n" );
		printf( "ico-1.0.4-x86_64-2\n" );
		printf( "icon-naming-utils-0.8.90-x86_64-2\n" );
		printf( "icu4c-56.1-x86_64-2\n" );
		printf( "idnkit-1.0-x86_64-1\n" );
		printf( "iftop-1.0pre2-x86_64-1\n" );
		printf( "ilmbase-2.2.0-x86_64-1\n" );
		printf( "imagemagick-6.9.4_9-x86_64-1\n" );
		printf( "imake-1.0.7-x86_64-2\n" );
		printf( "imapd-2.20-x86_64-2\n" );
		printf( "indent-2.2.10-x86_64-1\n" );
		printf( "inetd-1.79s-x86_64-9\n" );
		printf( "infozip-6.0-x86_64-3\n" );
		printf( "inotify-tools-3.14-x86_64-1\n" );
		printf( "inputproto-2.3.2-noarch-1\n" );
		printf( "intel-gpu-tools-1.9-x86_64-2\n" );
		printf( "intltool-0.51.0-x86_64-2\n" );
		printf( "iproute2-4.4.0-x86_64-1\n" );
		printf( "ipset-6.20-x86_64-1\n" );
		printf( "iptables-1.6.0-x86_64-2\n" );
		printf( "iptraf-ng-1.1.4-x86_64-1\n" );
		printf( "iputils-s20140519-x86_64-1\n" );
		printf( "ipw2100-fw-1.3-fw-1\n" );
		printf( "ipw2200-fw-3.1-fw-1\n" );
		printf( "irssi-0.8.19-x86_64-1\n" );
		printf( "iso-codes-3.67-noarch-1\n" );
		printf( "ispell-3.2.06-x86_64-1\n" );
		printf( "itstool-2.0.2-x86_64-1\n" );
		printf( "iw-4.3-x86_64-1\n" );
		printf( "jasper-1.900.1-x86_64-5\n" );
		printf( "jed-0.99_19-x86_64-2\n" );
		printf( "jemalloc-3.6.0-x86_64-1\n" );
		printf( "jfsutils-1.1.15-x86_64-1\n" );
		printf( "joe-4.1-x86_64-1\n" );
		printf( "jove-4.16.0.73-x86_64-1\n" );
		printf( "js185-1.0.0-x86_64-1\n" );
		printf( "json-c-0.12-x86_64-1\n" );
		printf( "judy-1.0.5-x86_64-1\n" );
		printf( "juk-4.14.3-x86_64-2\n" );
		printf( "k3b-2.0.3-x86_64-2\n" );
		printf( "kaccessible-4.14.3-x86_64-2\n" );
		printf( "kactivities-4.13.3-x86_64-2\n" );
		printf( "kajongg-4.14.3-x86_64-2\n" );
		printf( "kalgebra-4.14.3-x86_64-2\n" );
		printf( "kalzium-4.14.3-x86_64-2\n" );
		printf( "kamera-4.14.3-x86_64-2\n" );
		printf( "kanagram-4.14.3-x86_64-2\n" );
		printf( "kapman-4.14.3-x86_64-2\n" );
		printf( "kapptemplate-4.14.3-x86_64-2\n" );
		printf( "kate-4.14.3-x86_64-2\n" );
		printf( "katomic-4.14.3-x86_64-2\n" );
		printf( "kaudiocreator-1.3-x86_64-2\n" );
		printf( "kbd-1.15.3-x86_64-2\n" );
		printf( "kblackbox-4.14.3-x86_64-2\n" );
		printf( "kblocks-4.14.3-x86_64-2\n" );
		printf( "kbounce-4.14.3-x86_64-2\n" );
		printf( "kbproto-1.0.7-noarch-1\n" );
		printf( "kbreakout-4.14.3-x86_64-2\n" );
		printf( "kbruch-4.14.3-x86_64-2\n" );
		printf( "kcachegrind-4.14.3-x86_64-2\n" );
		printf( "kcalc-4.14.3-x86_64-2\n" );
		printf( "kcharselect-4.14.3-x86_64-2\n" );
		printf( "kcolorchooser-4.14.3-x86_64-2\n" );
		printf( "kcron-4.14.3-x86_64-2\n" );
		printf( "kde-base-artwork-4.14.3-x86_64-2\n" );
		printf( "kde-baseapps-4.14.3-x86_64-2\n" );
		printf( "kde-dev-scripts-4.14.3-x86_64-2\n" );
		printf( "kde-dev-utils-4.14.3-x86_64-2\n" );
		printf( "kde-runtime-4.14.3-x86_64-3\n" );
		printf( "kde-wallpapers-4.14.3-noarch-1\n" );
		printf( "kde-workspace-4.11.22-x86_64-4\n" );
		printf( "kdeartwork-4.14.3-x86_64-2\n" );
		printf( "kdeconnect-kde-0.8-x86_64-3\n" );
		printf( "kdegraphics-mobipocket-4.14.3-x86_64-2\n" );
		printf( "kdegraphics-strigi-analyzer-4.14.3-x86_64-2\n" );
		printf( "kdegraphics-thumbnailers-4.14.3-x86_64-2\n" );
		printf( "kdelibs-4.14.21-x86_64-1\n" );
		printf( "kdenetwork-filesharing-4.14.3-x86_64-2\n" );
		printf( "kdenetwork-strigi-analyzers-4.14.3-x86_64-2\n" );
		printf( "kdepim-4.14.10-x86_64-1\n" );
		printf( "kdepim-runtime-4.14.10-x86_64-1\n" );
		printf( "kdepimlibs-4.14.10-x86_64-2\n" );
		printf( "kdeplasma-addons-4.14.3-x86_64-2\n" );
		printf( "kdesdk-kioslaves-4.14.3-x86_64-2\n" );
		printf( "kdesdk-strigi-analyzers-4.14.3-x86_64-2\n" );
		printf( "kdesdk-thumbnailers-4.14.3-x86_64-2\n" );
		printf( "kdev-python-1.7.2-x86_64-1\n" );
		printf( "kdevelop-4.7.2-x86_64-1\n" );
		printf( "kdevelop-pg-qt-1.0.0-x86_64-2\n" );
		printf( "kdevelop-php-1.7.2-x86_64-1\n" );
		printf( "kdevelop-php-docs-1.7.2-x86_64-1\n" );
		printf( "kdevplatform-1.7.2-x86_64-1\n" );
		printf( "kdewebdev-4.14.3-x86_64-2\n" );
		printf( "kdf-4.14.3-x86_64-2\n" );
		printf( "kdiamond-4.14.3-x86_64-2\n" );
		printf( "kernel-firmware-20160628git-noarch-1\n" );
		printf( "kernel-generic-4.4.14-x86_64-1\n" );
		printf( "kernel-headers-4.4.14-x86-1\n" );
		printf( "kernel-huge-4.4.14-x86_64-1\n" );
		printf( "kernel-modules-4.4.14-x86_64-1\n" );
		printf( "kernel-source-4.4.14-noarch-1\n" );
		printf( "keybinder-0.3.1-x86_64-1\n" );
		printf( "keyutils-1.5.9-x86_64-1\n" );
		printf( "kfilemetadata-4.14.3-x86_64-2\n" );
		printf( "kfloppy-4.14.3-x86_64-2\n" );
		printf( "kfourinline-4.14.3-x86_64-2\n" );
		printf( "kgamma-4.14.3-x86_64-2\n" );
		printf( "kgeography-4.14.3-x86_64-2\n" );
		printf( "kget-4.14.3-x86_64-2\n" );
		printf( "kgoldrunner-4.14.3-x86_64-2\n" );
		printf( "kgpg-4.14.3-x86_64-2\n" );
		printf( "khangman-4.14.3-x86_64-2\n" );
		printf( "kig-4.14.3-x86_64-3\n" );
		printf( "kigo-4.14.3-x86_64-2\n" );
		printf( "killbots-4.14.3-x86_64-2\n" );
		printf( "kio-mtp-2063e75_20131020git-x86_64-3\n" );
		printf( "kiriki-4.14.3-x86_64-2\n" );
		printf( "kiten-4.14.3-x86_64-2\n" );
		printf( "kjumpingcube-4.14.3-x86_64-2\n" );
		printf( "klettres-4.14.3-x86_64-2\n" );
		printf( "klickety-4.14.3-x86_64-2\n" );
		printf( "klines-4.14.3-x86_64-2\n" );
		printf( "kmag-4.14.3-x86_64-2\n" );
		printf( "kmahjongg-4.14.3-x86_64-2\n" );
		printf( "kmines-4.14.3-x86_64-2\n" );
		printf( "kmix-4.14.3-x86_64-3\n" );
		printf( "kmod-22-x86_64-1\n" );
		printf( "kmousetool-4.14.3-x86_64-2\n" );
		printf( "kmouth-4.14.3-x86_64-2\n" );
		printf( "kmplot-4.14.3-x86_64-2\n" );
		printf( "knavalbattle-4.14.3-x86_64-2\n" );
		printf( "knetwalk-4.14.3-x86_64-2\n" );
		printf( "kolf-4.14.3-x86_64-2\n" );
		printf( "kollision-4.14.3-x86_64-2\n" );
		printf( "kolourpaint-4.14.3-x86_64-2\n" );
		printf( "kompare-4.14.3-x86_64-2\n" );
		printf( "konquest-4.14.3-x86_64-2\n" );
		printf( "konsole-4.14.3-x86_64-2\n" );
		printf( "kopete-4.14.3-x86_64-2\n" );
		printf( "korundum-4.14.3-x86_64-3\n" );
		printf( "kpat-4.14.3-x86_64-2\n" );
		printf( "kplayer-0.7.2-x86_64-2\n" );
		printf( "kppp-4.14.3-x86_64-2\n" );
		printf( "kqtquickcharts-4.14.3-x86_64-2\n" );
		printf( "krdc-4.14.3-x86_64-2\n" );
		printf( "kremotecontrol-4.14.3-x86_64-2\n" );
		printf( "kreversi-4.14.3-x86_64-2\n" );
		printf( "krfb-4.14.3-x86_64-2\n" );
		printf( "kross-interpreters-4.14.3-x86_64-2\n" );
		printf( "kruler-4.14.3-x86_64-2\n" );
		printf( "ksaneplugin-4.14.3-x86_64-2\n" );
		printf( "kscreen-1.0.2.1-x86_64-2\n" );
		printf( "ksh93-2012_08_01-x86_64-2\n" );
		printf( "kshisen-4.14.3-x86_64-2\n" );
		printf( "ksirk-4.14.3-x86_64-2\n" );
		printf( "ksnakeduel-4.14.3-x86_64-2\n" );
		printf( "ksnapshot-4.14.3-x86_64-2\n" );
		printf( "kspaceduel-4.14.3-x86_64-2\n" );
		printf( "ksquares-4.14.3-x86_64-2\n" );
		printf( "kstars-4.14.3-x86_64-2\n" );
		printf( "ksudoku-4.14.3-x86_64-2\n" );
		printf( "ksystemlog-4.14.3-x86_64-2\n" );
		printf( "kteatime-4.14.3-x86_64-2\n" );
		printf( "ktimer-4.14.3-x86_64-2\n" );
		printf( "ktorrent-4.3.1-x86_64-2\n" );
		printf( "ktouch-4.14.3-x86_64-3\n" );
		printf( "ktuberling-4.14.3-x86_64-2\n" );
		printf( "kturtle-4.14.3-x86_64-2\n" );
		printf( "ktux-4.14.3-x86_64-2\n" );
		printf( "kubrick-4.14.3-x86_64-2\n" );
		printf( "kuser-4.14.3-x86_64-2\n" );
		printf( "kwalletmanager-4.14.3-x86_64-2\n" );
		printf( "kwebkitpart-1.3.4-x86_64-2\n" );
		printf( "kwordquiz-4.14.3-x86_64-2\n" );
		printf( "lcms-1.19-x86_64-3\n" );
		printf( "lcms2-2.7-x86_64-2\n" );
		printf( "less-481-x86_64-1\n" );
		printf( "lftp-4.7.2-x86_64-1\n" );
		printf( "lha-114i-x86_64-1\n" );
		printf( "libFS-1.0.7-x86_64-1\n" );
		printf( "libICE-1.0.9-x86_64-2\n" );
		printf( "libSM-1.2.2-x86_64-2\n" );
		printf( "libX11-1.6.3-x86_64-2\n" );
		printf( "libXScrnSaver-1.2.2-x86_64-2\n" );
		printf( "libXau-1.0.8-x86_64-2\n" );
		printf( "libXaw-1.0.13-x86_64-1\n" );
		printf( "libXaw3d-1.6.2-x86_64-3\n" );
		printf( "libXaw3dXft-1.6.2d-x86_64-2\n" );
		printf( "libXcm-0.5.2-x86_64-2\n" );
		printf( "libXcomposite-0.4.4-x86_64-2\n" );
		printf( "libXcursor-1.1.14-x86_64-2\n" );
		printf( "libXdamage-1.1.4-x86_64-2\n" );
		printf( "libXdmcp-1.1.2-x86_64-2\n" );
		printf( "libXevie-1.0.3-x86_64-2\n" );
		printf( "libXext-1.3.3-x86_64-2\n" );
		printf( "libXfixes-5.0.2-x86_64-1\n" );
		printf( "libXfont-1.5.1-x86_64-2\n" );
		printf( "libXfontcache-1.0.5-x86_64-2\n" );
		printf( "libXft-2.3.2-x86_64-3\n" );
		printf( "libXi-1.7.6-x86_64-1\n" );
		printf( "libXinerama-1.1.3-x86_64-2\n" );
		printf( "libXmu-1.1.2-x86_64-2\n" );
		printf( "libXp-1.0.3-x86_64-2\n" );
		printf( "libXpm-3.5.11-x86_64-2\n" );
		printf( "libXpresent-1.0.0-x86_64-1\n" );
		printf( "libXrandr-1.5.0-x86_64-1\n" );
		printf( "libXrender-0.9.9-x86_64-1\n" );
		printf( "libXres-1.0.7-x86_64-2\n" );
		printf( "libXt-1.1.5-x86_64-1\n" );
		printf( "libXtst-1.2.2-x86_64-2\n" );
		printf( "libXv-1.0.10-x86_64-2\n" );
		printf( "libXvMC-1.0.9-x86_64-2\n" );
		printf( "libXxf86dga-1.1.4-x86_64-2\n" );
		printf( "libXxf86misc-1.0.3-x86_64-2\n" );
		printf( "libXxf86vm-1.1.4-x86_64-2\n" );
		printf( "libaio-0.3.109-x86_64-1\n" );
		printf( "libao-1.2.0-x86_64-3\n" );
		printf( "libarchive-3.2.1-x86_64-1\n" );
		printf( "libart_lgpl-2.3.21-x86_64-1\n" );
		printf( "libassuan-2.4.2-x86_64-1\n" );
		printf( "libasyncns-0.8-x86_64-1\n" );
		printf( "libatasmart-0.19-x86_64-2\n" );
		printf( "libbluedevil-2.1-x86_64-1\n" );
		printf( "libcaca-0.99.beta18-x86_64-2\n" );
		printf( "libcanberra-0.30-x86_64-5\n" );
		printf( "libcap-2.22-x86_64-1\n" );
		printf( "libcap-ng-0.7.7-x86_64-1\n" );
		printf( "libcddb-1.3.2-x86_64-3\n" );
		printf( "libcdio-0.93-x86_64-1\n" );
		printf( "libcdio-paranoia-10.2+0.93+1-x86_64-1\n" );
		printf( "libcgroup-0.41-x86_64-1\n" );
		printf( "libcroco-0.6.11-x86_64-1\n" );
		printf( "libdbusmenu-qt-0.9.2-x86_64-2\n" );
		printf( "libdiscid-0.6.1-x86_64-1\n" );
		printf( "libdmx-1.1.3-x86_64-2\n" );
		printf( "libdrm-2.4.68-x86_64-1\n" );
		printf( "libdvdnav-5.0.3-x86_64-1\n" );
		printf( "libdvdread-5.0.3-x86_64-1\n" );
		printf( "libepoxy-1.3.1-x86_64-1\n" );
		printf( "liberation-fonts-ttf-1.07.4-noarch-1\n" );
		printf( "libevdev-1.4.1-x86_64-1\n" );
		printf( "libevent-2.0.22-x86_64-1\n" );
		printf( "libexif-0.6.21-x86_64-1\n" );
		printf( "libfakekey-0.1-x86_64-1\n" );
		printf( "libffi-3.2.1-x86_64-1\n" );
		printf( "libfontenc-1.1.3-x86_64-1\n" );
		printf( "libgcrypt-1.7.1-x86_64-1\n" );
		printf( "libglade-2.6.4-x86_64-5\n" );
		printf( "libgnome-keyring-3.12.0-x86_64-1\n" );
		printf( "libgpg-error-1.23-x86_64-1\n" );
		printf( "libgphoto2-2.5.10-x86_64-1\n" );
		printf( "libgpod-0.8.3-x86_64-2\n" );
		printf( "libgsf-1.14.36-x86_64-1\n" );
		printf( "libgudev-230-x86_64-1\n" );
		printf( "libhangul-0.1.0-x86_64-1\n" );
		printf( "libical-2.0.0-x86_64-1\n" );
		printf( "libid3tag-0.15.1b-x86_64-4\n" );
		printf( "libidl-0.8.14-x86_64-1\n" );
		printf( "libidn-1.30-x86_64-1\n" );
		printf( "libieee1284-0.2.11-x86_64-3\n" );
		printf( "libimobiledevice-1.2.0-x86_64-1\n" );
		printf( "libiodbc-3.52.10-x86_64-2\n" );
		printf( "libjpeg-turbo-1.5.0-x86_64-1\n" );
		printf( "libkarma-0.1.1-x86_64-2\n" );
		printf( "libkcddb-4.14.3-x86_64-2\n" );
		printf( "libkcompactdisc-4.14.3-x86_64-2\n" );
		printf( "libkdcraw-4.14.3-x86_64-3\n" );
		printf( "libkdeedu-4.14.3-x86_64-2\n" );
		printf( "libkdegames-4.14.3-x86_64-2\n" );
		printf( "libkexiv2-4.14.3-x86_64-2\n" );
		printf( "libkipi-4.14.3-x86_64-2\n" );
		printf( "libkmahjongg-4.14.3-x86_64-2\n" );
		printf( "libkomparediff2-4.14.3-x86_64-2\n" );
		printf( "libksane-4.14.3-x86_64-2\n" );
		printf( "libksba-1.3.3-x86_64-1\n" );
		printf( "libkscreen-1.0.5-x86_64-2\n" );
		printf( "libktorrent-1.3.1-x86_64-3\n" );
		printf( "liblastfm-1.0.9-x86_64-1\n" );
		printf( "libmad-0.15.1b-x86_64-3\n" );
		printf( "libmbim-1.12.2-x86_64-2\n" );
		printf( "libmcrypt-2.5.8-x86_64-1\n" );
		printf( "libmcs-0.7.2-x86_64-1\n" );
		printf( "libmm-qt-1.0.1-x86_64-2\n" );
		printf( "libmng-2.0.3-x86_64-1\n" );
		printf( "libmnl-1.0.3-x86_64-1\n" );
		printf( "libmowgli-0.7.1-x86_64-1\n" );
		printf( "libmpc-1.0.3-x86_64-1\n" );
		printf( "libmsn-4.2.1-x86_64-2\n" );
		printf( "libmtp-1.1.11-x86_64-1\n" );
		printf( "libndp-1.6-x86_64-1\n" );
		printf( "libnetfilter_acct-1.0.2-x86_64-1\n" );
		printf( "libnetfilter_conntrack-1.0.5-x86_64-1\n" );
		printf( "libnetfilter_cthelper-1.0.0-x86_64-1\n" );
		printf( "libnetfilter_cttimeout-1.0.0-x86_64-1\n" );
		printf( "libnetfilter_log-1.0.1-x86_64-1\n" );
		printf( "libnetfilter_queue-1.0.2-x86_64-1\n" );
		printf( "libnfnetlink-1.0.1-x86_64-1\n" );
		printf( "libnftnl-1.0.6-x86_64-1\n" );
		printf( "libnih-1.0.3-x86_64-2\n" );
		printf( "libnjb-2.2.6-x86_64-5\n" );
		printf( "libnl-1.1.4-x86_64-1\n" );
		printf( "libnl3-3.2.27-x86_64-1\n" );
		printf( "libnm-qt-0.9.8.3-x86_64-2\n" );
		printf( "libnotify-0.7.6-x86_64-1\n" );
		printf( "libodfgen-0.1.6-x86_64-1\n" );
		printf( "libogg-1.3.2-x86_64-1\n" );
		printf( "liboggz-1.1.1-x86_64-1\n" );
		printf( "liboil-0.3.17-x86_64-1\n" );
		printf( "libpcap-1.7.4-x86_64-1\n" );
		printf( "libpciaccess-0.13.4-x86_64-1\n" );
		printf( "libplist-1.12-x86_64-1\n" );
		printf( "libpng-1.6.23-x86_64-1\n" );
		printf( "libproxy-0.4.12-x86_64-1\n" );
		printf( "libpthread-stubs-0.3-noarch-1\n" );
		printf( "libqmi-1.12.6-x86_64-1\n" );
		printf( "libraw1394-2.1.1-x86_64-1\n" );
		printf( "librevenge-0.0.4-x86_64-1\n" );
		printf( "librsvg-2.40.16-x86_64-1\n" );
		printf( "libsamplerate-0.1.8-x86_64-1\n" );
		printf( "libsecret-0.18.5-x86_64-1\n" );
		printf( "libsigc++-2.6.2-x86_64-1\n" );
		printf( "libsigsegv-2.10-x86_64-1\n" );
		printf( "libsndfile-1.0.26-x86_64-1\n" );
		printf( "libsoup-2.52.2-x86_64-2\n" );
		printf( "libspectre-0.2.7-x86_64-1\n" );
		printf( "libssh-0.7.3-x86_64-1\n" );
		printf( "libssh2-1.7.0-x86_64-1\n" );
		printf( "libtasn1-4.8-x86_64-1\n" );
		printf( "libtermcap-1.2.3-x86_64-7\n" );
		printf( "libtheora-1.1.1-x86_64-1\n" );
		printf( "libtiff-4.0.6-x86_64-1\n" );
		printf( "libtirpc-1.0.1-x86_64-2\n" );
		printf( "libtool-2.4.6-x86_64-4\n" );
		printf( "libunistring-0.9.3-x86_64-1\n" );
		printf( "libusb-1.0.20-x86_64-1\n" );
		printf( "libusb-compat-0.1.5-x86_64-2\n" );
		printf( "libusbmuxd-1.0.10-x86_64-1\n" );
		printf( "libva-1.6.2-x86_64-1\n" );
		printf( "libva-intel-driver-1.6.2-x86_64-1\n" );
		printf( "libvdpau-1.1.1-x86_64-1\n" );
		printf( "libvisio-0.1.5-x86_64-1\n" );
		printf( "libvisual-0.4.0-x86_64-3\n" );
		printf( "libvisual-plugins-0.4.0-x86_64-2\n" );
		printf( "libvncserver-0.9.10-x86_64-2\n" );
		printf( "libvorbis-1.3.5-x86_64-1\n" );
		printf( "libvpx-1.5.0-x86_64-1\n" );
		printf( "libwmf-0.2.8.4-x86_64-6\n" );
		printf( "libwmf-docs-0.2.8.4-noarch-6\n" );
		printf( "libwnck-2.31.0-x86_64-2\n" );
		printf( "libwpd-0.10.1-x86_64-1\n" );
		printf( "libwpg-0.3.1-x86_64-1\n" );
		printf( "libx86-1.1-x86_64-2\n" );
		printf( "libxcb-1.11.1-x86_64-1\n" );
		printf( "libxkbfile-1.0.9-x86_64-1\n" );
		printf( "libxklavier-5.4-x86_64-1\n" );
		printf( "libxml2-2.9.4-x86_64-2\n" );
		printf( "libxshmfence-1.2-x86_64-2\n" );
		printf( "libxslt-1.1.29-x86_64-1\n" );
		printf( "libyaml-0.1.6-x86_64-1\n" );
		printf( "libzip-1.0.1-x86_64-2\n" );
		printf( "lilo-24.2-x86_64-2\n" );
		printf( "links-2.12-x86_64-2\n" );
		printf( "linuxdoc-tools-0.9.69-x86_64-5\n" );
		printf( "listres-1.0.3-x86_64-2\n" );
		printf( "llvm-3.8.0-x86_64-2\n" );
		printf( "lm_sensors-3.4.0-x86_64-1\n" );
		printf( "lndir-1.0.3-x86_64-2\n" );
		printf( "logrotate-3.8.9-x86_64-1\n" );
		printf( "lokalize-4.14.3-x86_64-2\n" );
		printf( "loudmouth-1.5.2-x86_64-1\n" );
		printf( "lrzip-0.621-x86_64-1\n" );
		printf( "lskat-4.14.3-x86_64-3\n" );
		printf( "lsof-4.89-x86_64-1\n" );
		printf( "lsscsi-0.28-x86_64-1\n" );
		printf( "luit-1.1.1-x86_64-2\n" );
		printf( "lvm2-2.02.154-x86_64-1\n" );
		printf( "lxc-2.0.1-x86_64-4\n" );
		printf( "lynx-2.8.8rel.2-x86_64-1\n" );
		printf( "lzip-1.16-x86_64-1\n" );
		printf( "lzo-2.09-x86_64-1\n" );
		printf( "m17n-lib-1.6.1-x86_64-1\n" );
		printf( "m4-1.4.17-x86_64-1\n" );
		printf( "madplay-0.15.2b-x86_64-4\n" );
		printf( "mailx-12.5-x86_64-2\n" );
		printf( "make-4.1-x86_64-2\n" );
		printf( "makedepend-1.0.5-x86_64-2\n" );
		printf( "man-1.6g-x86_64-3\n" );
		printf( "man-pages-4.06-noarch-1\n" );
		printf( "marble-4.14.3-x86_64-2\n" );
		printf( "mariadb-10.0.26-x86_64-1\n" );
		printf( "mc-4.8.16-x86_64-2\n" );
		printf( "mcabber-1.0.1-x86_64-1\n" );
		printf( "mcelog-128-x86_64-1\n" );
		printf( "mdadm-3.3.4-x86_64-1\n" );
		printf( "media-player-info-18-noarch-1\n" );
		printf( "mercurial-3.8.2-x86_64-1\n" );
		printf( "mesa-11.2.2-x86_64-1\n" );
		printf( "metamail-2.7-x86_64-5\n" );
		printf( "mhash-0.9.9.9-x86_64-3\n" );
		printf( "minicom-2.6.2-x86_64-1\n" );
		printf( "mkcomposecache-1.2.1-x86_64-2\n" );
		printf( "mkfontdir-1.0.7-noarch-1\n" );
		printf( "mkfontscale-1.1.2-x86_64-2\n" );
		printf( "mkinitrd-1.4.8-x86_64-8\n" );
		printf( "mm-1.4.2-x86_64-2\n" );
		printf( "mobile-broadband-provider-info-20151214-x86_64-1\n" );
		printf( "moc-2.5.1-x86_64-1\n" );
		printf( "most-5.0.0a-x86_64-2\n" );
		printf( "motif-2.3.5-x86_64-1\n" );
		printf( "mozilla-firefox-45.2.0esr-x86_64-1\n" );
		printf( "mozilla-nss-3.23-x86_64-1\n" );
		printf( "mozilla-thunderbird-45.1.1-i586-1\n" );
		printf( "mozilla-thunderbird-45.1.1-x86_64-1\n" );
		printf( "mpfr-3.1.4-x86_64-1\n" );
		printf( "mpg123-1.23.4-i586-1\n" );
		printf( "mpg123-1.23.4-x86_64-1\n" );
		printf( "mplayerthumbs-4.14.3-x86_64-2\n" );
		printf( "mt-st-0.9b-x86_64-2\n" );
		printf( "mtdev-1.1.5-x86_64-1\n" );
		printf( "mtr-0.86-x86_64-1\n" );
		printf( "mtx-1.3.12-x86_64-1\n" );
		printf( "mutt-1.6.1-x86_64-1\n" );
		printf( "nano-2.6.0-x86_64-1\n" );
		printf( "nasm-2.12.01-x86_64-1\n" );
		printf( "nc-1.10-x86_64-1\n" );
		printf( "ncftp-3.2.5-x86_64-1\n" );
		printf( "ncompress-4.2.4.4-x86_64-1\n" );
		printf( "ncurses-5.9-x86_64-4\n" );
		printf( "neon-0.30.1-x86_64-2\n" );
		printf( "nepomuk-core-4.14.3-x86_64-2\n" );
		printf( "nepomuk-widgets-4.14.3-x86_64-2\n" );
		printf( "net-snmp-5.7.3-x86_64-3\n" );
		printf( "net-tools-1.60.20120726git-x86_64-1\n" );
		printf( "netatalk-2.2.3-x86_64-6\n" );
		printf( "netdate-bsd4-x86_64-1\n" );
		printf( "netkit-bootparamd-0.17-x86_64-2\n" );
		printf( "netkit-ftp-0.17-x86_64-2\n" );
		printf( "netkit-ntalk-0.17-x86_64-3\n" );
		printf( "netkit-routed-0.17-x86_64-1\n" );
		printf( "netkit-rsh-0.17-x86_64-1\n" );
		printf( "netkit-rusers-0.17-x86_64-1\n" );
		printf( "netkit-rwall-0.17-x86_64-1\n" );
		printf( "netkit-rwho-0.17-x86_64-2\n" );
		printf( "netkit-timed-0.17-x86_64-1\n" );
		printf( "netpbm-10.66.02-x86_64-4\n" );
		printf( "netpipes-4.2-x86_64-1\n" );
		printf( "nettle-3.2-x86_64-1\n" );
		printf( "netwatch-1.3.1_2-x86_64-1\n" );
		printf( "network-manager-applet-1.2.2-x86_64-1\n" );
		printf( "network-scripts-14.2-noarch-1\n" );
		printf( "netwrite-0.17-x86_64-1\n" );
		printf( "newspost-2.1.1-x86_64-1\n" );
		printf( "newt-0.52.19-x86_64-1\n" );
		printf( "nfacct-1.0.1-x86_64-1\n" );
		printf( "nfs-utils-1.3.3-x86_64-2\n" );
		printf( "nftables-0.6-x86_64-1\n" );
		printf( "nmap-7.12-x86_64-1\n" );
		printf( "nn-6.7.3-x86_64-3\n" );
		printf( "normalize-0.7.7-x86_64-2\n" );
		printf( "notify-python-0.1.1-x86_64-5\n" );
		printf( "ntfs-3g-2016.2.22-x86_64-1\n" );
		printf( "ntp-4.2.8p8-x86_64-1\n" );
		printf( "obexftp-0.24-x86_64-1\n" );
		printf( "oclock-1.0.3-x86_64-2\n" );
		printf( "okteta-4.14.3-x86_64-2\n" );
		printf( "okular-4.14.3-x86_64-2\n" );
		printf( "openexr-2.2.0-x86_64-1\n" );
		printf( "openjpeg-2.1.0-x86_64-1\n" );
		printf( "openldap-client-2.4.42-x86_64-1\n" );
		printf( "openobex-1.7.1-x86_64-1\n" );
		printf( "openssh-7.2p2-x86_64-1\n" );
		printf( "openssl-1.0.2h-x86_64-1\n" );
		printf( "openssl-solibs-1.0.2h-x86_64-1\n" );
		printf( "openvpn-2.3.11-x86_64-1\n" );
		printf( "oprofile-1.1.0-x86_64-1\n" );
		printf( "orc-0.4.24-x86_64-1\n" );
		printf( "os-prober-1.70-x86_64-1\n" );
		printf( "oxygen-gtk2-1.4.6-x86_64-2\n" );
		printf( "oxygen-icons-4.14.3-x86_64-2\n" );
		printf( "p11-kit-0.23.2-x86_64-1\n" );
		printf( "p2c-1.21alpha2-x86_64-3\n" );
		printf( "pairs-4.14.3-x86_64-2\n" );
		printf( "palapeli-4.14.3-x86_64-2\n" );
		printf( "pamixer-1.3.1-x86_64-1\n" );
		printf( "pan-0.139-x86_64-3\n" );
		printf( "pango-1.38.1-x86_64-1\n" );
		printf( "pangomm-2.38.1-x86_64-1\n" );
		printf( "parley-4.14.3-x86_64-2\n" );
		printf( "parted-3.2-x86_64-2\n" );
		printf( "partitionmanager-1.1.1-x86_64-2\n" );
		printf( "patch-2.7.5-x86_64-1\n" );
		printf( "pavucontrol-3.0-x86_64-1\n" );
		printf( "pciutils-3.4.1-x86_64-2\n" );
		printf( "pcmciautils-018-x86_64-1\n" );
		printf( "pcre-8.39-x86_64-1\n" );
		printf( "perl-5.22.2-x86_64-1\n" );
		printf( "perlkde-4.14.3-x86_64-2\n" );
		printf( "perlqt-4.14.3-x86_64-2\n" );
		printf( "phonon-4.8.3-x86_64-2\n" );
		printf( "phonon-gstreamer-4.8.2-x86_64-1\n" );
		printf( "php-5.6.23-x86_64-1\n" );
		printf( "picmi-4.14.3-x86_64-2\n" );
		printf( "pidentd-3.0.19-x86_64-2\n" );
		printf( "pidgin-2.10.12-x86_64-2\n" );
		printf( "pilot-link-0.12.5-x86_64-10\n" );
		printf( "pinentry-0.9.7-x86_64-1\n" );
		printf( "pixman-0.34.0-x86_64-1\n" );
		printf( "pkg-config-0.29.1-x86_64-2\n" );
		printf( "pkgtools-14.2-noarch-10\n" );
		printf( "plasma-nm-0.9.3.6-x86_64-1\n" );
		printf( "pm-utils-1.4.1-x86_64-5\n" );
		printf( "pmake-1.111-x86_64-3\n" );
		printf( "polkit-0.113-x86_64-2\n" );
		printf( "polkit-gnome-0.105-x86_64-1\n" );
		printf( "polkit-kde-agent-1-9d74ae3_20120104git-x86_64-2\n" );
		printf( "polkit-kde-kcmodules-1-001bdf7_20120111git-x86_64-2\n" );
		printf( "polkit-qt-1-0.103.0-x86_64-1\n" );
		printf( "popa3d-1.0.3-x86_64-1\n" );
		printf( "poppler-0.45.0-x86_64-1\n" );
		printf( "poppler-data-0.4.7-noarch-1\n" );
		printf( "popt-1.16-x86_64-2\n" );
		printf( "powertop-2.8-x86_64-1\n" );
		printf( "poxml-4.14.3-x86_64-2\n" );
		printf( "ppp-2.4.7-x86_64-1\n" );
		printf( "presentproto-1.0-x86_64-2\n" );
		printf( "print-manager-4.14.3-x86_64-2\n" );
		printf( "printproto-1.0.5-noarch-1\n" );
		printf( "procmail-3.22-x86_64-2\n" );
		printf( "procps-ng-3.3.11-x86_64-1\n" );
		printf( "proftpd-1.3.5b-x86_64-1\n" );
		printf( "pssh-2.3.1-x86_64-1\n" );
		printf( "pth-2.0.7-x86_64-1\n" );
		printf( "pulseaudio-9.0-x86_64-1\n" );
		printf( "pycairo-1.10.0-x86_64-1\n" );
		printf( "pycups-1.9.73-x86_64-1\n" );
		printf( "pycurl-7.43.0-x86_64-1\n" );
		printf( "pygobject-2.28.6-x86_64-2\n" );
		printf( "pygobject3-3.18.2-x86_64-1\n" );
		printf( "pygtk-2.24.0-x86_64-2\n" );
		printf( "pykde4-4.14.3-x86_64-3\n" );
		printf( "pyrex-0.9.9-x86_64-2\n" );
		printf( "python-2.7.11-x86_64-2\n" );
		printf( "python-pillow-3.0.0-x86_64-1\n" );
		printf( "python-setuptools-22.0.5-x86_64-1\n" );
		printf( "qca-2.1.1-x86_64-2\n" );
		printf( "qimageblitz-0.0.6-x86_64-1\n" );
		printf( "qjson-0.8.1-x86_64-1\n" );
		printf( "qpdf-6.0.0-x86_64-1\n" );
		printf( "qt-4.8.7-x86_64-4\n" );
		printf( "qt-gstreamer-1.2.0-x86_64-1\n" );
		printf( "qtruby-4.14.3-x86_64-4\n" );
		printf( "qtscriptgenerator-0.2.0-x86_64-2\n" );
		printf( "quota-4.03-x86_64-1\n" );
		printf( "radeontool-1.6.3-x86_64-1\n" );
		printf( "randrproto-1.5.0-noarch-1\n" );
		printf( "raptor2-2.0.15-x86_64-2\n" );
		printf( "rasqal-0.9.30-x86_64-1\n" );
		printf( "rcs-5.9.4-x86_64-1\n" );
		printf( "rdesktop-1.8.3-x86_64-1\n" );
		printf( "rdist-6.1.5-x86_64-2\n" );
		printf( "readline-6.3-x86_64-2\n" );
		printf( "recordproto-1.14.2-noarch-1\n" );
		printf( "redland-1.0.16-x86_64-1\n" );
		printf( "reiserfsprogs-3.6.24-x86_64-1\n" );
		printf( "rendercheck-1.5-x86_64-1\n" );
		printf( "renderproto-0.11.1-noarch-1\n" );
		printf( "resourceproto-1.2.0-noarch-1\n" );
		printf( "rfkill-0.5-x86_64-1\n" );
		printf( "rgb-1.0.6-x86_64-2\n" );
		printf( "rocs-4.14.3-x86_64-2\n" );
		printf( "rp-pppoe-3.12-x86_64-1\n" );
		printf( "rpcbind-0.2.3-x86_64-1\n" );
		printf( "rpm-4.12.0.1-x86_64-1\n" );
		printf( "rpm2tgz-1.2.2-x86_64-1\n" );
		printf( "rsync-3.1.2-x86_64-1\n" );
		printf( "ruby-2.2.5-x86_64-1\n" );
		printf( "rxvt-2.7.10-x86_64-4\n" );
		printf( "rzip-2.1-x86_64-1\n" );
		printf( "samba-4.4.4-x86_64-3\n" );
		printf( "sane-1.0.25-x86_64-2\n" );
		printf( "sazanami-fonts-ttf-20040629-noarch-1\n" );
		printf( "sbc-1.3-x86_64-1\n" );
		printf( "sc-7.16-x86_64-4\n" );
		printf( "scim-1.4.15-x86_64-2\n" );
		printf( "scim-anthy-1.2.7-x86_64-1\n" );
		printf( "scim-hangul-0.4.0-x86_64-1\n" );
		printf( "scim-input-pad-0.1.3.1-x86_64-1\n" );
		printf( "scim-m17n-0.2.3-x86_64-1\n" );
		printf( "scim-pinyin-0.5.92-x86_64-1\n" );
		printf( "scim-tables-0.5.14.1-x86_64-1\n" );
		printf( "scons-2.4.1-x86_64-1\n" );
		printf( "screen-4.4.0-x86_64-1\n" );
		printf( "scrnsaverproto-1.2.2-noarch-1\n" );
		printf( "sdl-1.2.15-x86_64-4\n" );
		printf( "sdparm-1.10-x86_64-1\n" );
		printf( "seamonkey-2.40-x86_64-1\n" );
		printf( "seamonkey-solibs-2.40-x86_64-1\n" );
		printf( "sed-4.2.2-x86_64-1\n" );
		printf( "seejpeg-1.10-x86_64-1\n" );
		printf( "sendmail-8.15.2-x86_64-2\n" );
		printf( "sendmail-cf-8.15.2-noarch-2\n" );
		printf( "serf-1.3.8-x86_64-1\n" );
		printf( "sessreg-1.1.0-x86_64-2\n" );
		printf( "setxkbmap-1.3.1-x86_64-1\n" );
		printf( "seyon-2.20c-x86_64-3\n" );
		printf( "sg3_utils-1.42-x86_64-1\n" );
		printf( "shadow-4.2.1-x86_64-1\n" );
		printf( "shared-desktop-ontologies-0.11.0-x86_64-1\n" );
		printf( "shared-mime-info-1.6-x86_64-1\n" );
		printf( "sharutils-4.15.2-x86_64-1\n" );
		printf( "showfont-1.0.5-x86_64-2\n" );
		printf( "sinhala_lklug-font-ttf-20060929-noarch-1\n" );
		printf( "sip-4.18-x86_64-1\n" );
		printf( "skanlite-1.1-x86_64-2\n" );
		printf( "slackpkg-2.82.1-noarch-3\n" );
		printf( "slacktrack-2.17-x86_64-1\n" );
		printf( "slang-2.3.0-x86_64-1\n" );
		printf( "slang1-1.4.9-x86_64-1\n" );
		printf( "slocate-3.1-x86_64-4\n" );
		printf( "slrn-1.0.2-x86_64-1\n" );
		printf( "smartmontools-6.5-x86_64-1\n" );
		printf( "smokegen-4.14.3-x86_64-2\n" );
		printf( "smokekde-4.14.3-x86_64-2\n" );
		printf( "smokeqt-4.14.3-x86_64-3\n" );
		printf( "smproxy-1.0.6-x86_64-1\n" );
		printf( "snownews-1.5.12-x86_64-2\n" );
		printf( "soma-2.10.4-noarch-1\n" );
		printf( "soprano-2.9.4-x86_64-1\n" );
		printf( "sound-theme-freedesktop-0.7-noarch-1\n" );
		printf( "sox-14.4.2-x86_64-3\n" );
		printf( "speexdsp-1.2rc3-x86_64-1\n" );
		printf( "splitvt-1.6.5-x86_64-1\n" );
		printf( "sqlite-3.13.0-x86_64-1\n" );
		printf( "squashfs-tools-4.3-x86_64-1\n" );
		printf( "startup-notification-0.12-x86_64-2\n" );
		printf( "step-4.14.3-x86_64-4\n" );
		printf( "strace-4.11-x86_64-1\n" );
		printf( "strigi-0.7.8-x86_64-2\n" );
		printf( "stunnel-5.31-x86_64-1\n" );
		printf( "subversion-1.9.4-i586-1\n" );
		printf( "subversion-1.9.4-x86_64-1\n" );
		printf( "sudo-1.8.16-x86_64-1\n" );
		printf( "superkaramba-4.14.3-x86_64-2\n" );
		printf( "svgalib-1.9.25-x86_64-3\n" );
		printf( "svgpart-4.14.3-x86_64-2\n" );
		printf( "sweeper-4.14.3-x86_64-2\n" );
		printf( "swig-3.0.7-x86_64-1\n" );
		printf( "sysfsutils-2.1.0-x86_64-1\n" );
		printf( "sysklogd-1.5.1-x86_64-2\n" );
		printf( "syslinux-4.07-x86_64-1\n" );
		printf( "sysstat-11.2.1.1-x86_64-1\n" );
		printf( "system-config-printer-1.3.13-x86_64-2\n" );
		printf( "sysvinit-2.88dsf-x86_64-4\n" );
		printf( "sysvinit-functions-8.53-x86_64-2\n" );
		printf( "sysvinit-scripts-2.0-noarch-33\n" );
		printf( "t1lib-5.1.2-x86_64-3\n" );
		printf( "taglib-1.10-x86_64-1\n" );
		printf( "taglib-extras-1.0.1-x86_64-1\n" );
		printf( "tango-icon-theme-0.8.90-noarch-1\n" );
		printf( "tango-icon-theme-extras-0.1.0-noarch-1\n" );
		printf( "tar-1.29-x86_64-1\n" );
		printf( "tcl-8.6.5-x86_64-2\n" );
		printf( "tclx-8.4.1-x86_64-3\n" );
		printf( "tcp_wrappers-7.6-x86_64-1\n" );
		printf( "tcpdump-4.7.4-x86_64-1\n" );
		printf( "tcsh-6.19.00-x86_64-1\n" );
		printf( "telnet-0.17-x86_64-2\n" );
		printf( "terminus-font-4.40-noarch-1\n" );
		printf( "tetex-3.0-x86_64-10\n" );
		printf( "tetex-doc-3.0-x86_64-10\n" );
		printf( "texinfo-6.1-x86_64-1\n" );
		printf( "tftp-hpa-5.2-x86_64-2\n" );
		printf( "tibmachuni-font-ttf-1.901b-noarch-1\n" );
		printf( "time-1.7-x86_64-1\n" );
		printf( "tin-2.2.1-x86_64-3\n" );
		printf( "tix-8.4.3-x86_64-3\n" );
		printf( "tk-8.6.5-x86_64-2\n" );
		printf( "tmux-2.1-x86_64-3\n" );
		printf( "traceroute-2.0.21-x86_64-1\n" );
		printf( "transfig-3.2.5e-x86_64-3\n" );
		printf( "transset-1.0.1-x86_64-2\n" );
		printf( "tree-1.7.0-x86_64-1\n" );
		printf( "trn-3.6-x86_64-2\n" );
		printf( "ttf-indic-fonts-0.5.14-noarch-1\n" );
		printf( "twm-1.0.9-x86_64-1\n" );
		printf( "udisks-1.0.5-x86_64-3\n" );
		printf( "udisks2-2.1.5-x86_64-2\n" );
		printf( "ulogd-2.0.5-x86_64-1\n" );
		printf( "umbrello-4.14.3-x86_64-2\n" );
		printf( "unarj-265-x86_64-1\n" );
		printf( "upower-0.9.23-x86_64-3\n" );
		printf( "urwid-1.0.3-x86_64-1\n" );
		printf( "usb_modeswitch-2.2.6-x86_64-1\n" );
		printf( "usbmuxd-1.1.0-x86_64-1\n" );
		printf( "usbutils-008-x86_64-1\n" );
		printf( "utempter-1.1.6-x86_64-2\n" );
		printf( "util-linux-2.27.1-x86_64-1\n" );
		printf( "util-macros-1.19.0-noarch-1\n" );
		printf( "uucp-1.07-x86_64-1\n" );
		printf( "v4l-utils-1.10.0-x86_64-1\n" );
		printf( "vbetool-1.2.2-x86_64-1\n" );
		printf( "videoproto-2.3.3-noarch-1\n" );
		printf( "viewres-1.0.4-x86_64-2\n" );
		printf( "vim-7.4.1938-x86_64-1\n" );
		printf( "vim-gvim-7.4.1938-x86_64-1\n" );
		printf( "virtuoso-ose-6.1.8-x86_64-3\n" );
		printf( "vlan-1.9-x86_64-2\n" );
		printf( "vorbis-tools-1.4.0-x86_64-1\n" );
		printf( "vsftpd-3.0.3-x86_64-3\n" );
		printf( "vte-0.28.2-x86_64-4\n" );
		printf( "wavpack-4.75.2-x86_64-1\n" );
		printf( "wget-1.18-x86_64-1\n" );
		printf( "which-2.21-x86_64-1\n" );
		printf( "whois-5.2.12-x86_64-1\n" );
		printf( "wicd-kde-0.3.0_bcf27d8-x86_64-2\n" );
		printf( "windowmaker-0.95.7-x86_64-2\n" );
		printf( "wireless-tools-29-x86_64-9\n" );
		printf( "workbone-2.40-x86_64-4\n" );
		printf( "wpa_supplicant-2.5-x86_64-1\n" );
		printf( "wqy-zenhei-font-ttf-0.8.38_1-noarch-2\n" );
		printf( "x11-skel-7.7-x86_64-1\n" );
		printf( "x11-ssh-askpass-1.2.4.1-x86_64-2\n" );
		printf( "x11perf-1.6.0-x86_64-1\n" );
		printf( "x3270-3.3.12ga7-x86_64-2\n" );
		printf( "xaos-3.6-x86_64-1\n" );
		printf( "xapian-core-1.2.22-x86_64-1\n" );
		printf( "xauth-1.0.9-x86_64-2\n" );
		printf( "xbacklight-1.2.1-x86_64-2\n" );
		printf( "xbiff-1.0.3-x86_64-2\n" );
		printf( "xbitmaps-1.1.1-x86_64-2\n" );
		printf( "xcalc-1.0.6-x86_64-2\n" );
		printf( "xcb-proto-1.11-x86_64-2\n" );
		printf( "xcb-util-0.4.0-x86_64-2\n" );
		printf( "xcb-util-cursor-0.1.2-x86_64-2\n" );
		printf( "xcb-util-errors-1.0-x86_64-1\n" );
		printf( "xcb-util-image-0.4.0-x86_64-2\n" );
		printf( "xcb-util-keysyms-0.4.0-x86_64-2\n" );
		printf( "xcb-util-renderutil-0.3.9-x86_64-2\n" );
		printf( "xcb-util-wm-0.4.1-x86_64-2\n" );
		printf( "xclipboard-1.1.3-x86_64-2\n" );
		printf( "xclock-1.0.7-x86_64-2\n" );
		printf( "xcm-0.5.2-x86_64-2\n" );
		printf( "xcmiscproto-1.2.2-noarch-1\n" );
		printf( "xcmsdb-1.0.5-x86_64-1\n" );
		printf( "xcompmgr-1.1.7-x86_64-1\n" );
		printf( "xconsole-1.0.6-x86_64-2\n" );
		printf( "xcursor-themes-1.0.4-noarch-1\n" );
		printf( "xcursorgen-1.0.6-x86_64-3\n" );
		printf( "xdbedizzy-1.1.0-x86_64-2\n" );
		printf( "xdg-user-dirs-0.15-x86_64-1\n" );
		printf( "xdg-utils-1.1.1-noarch-1\n" );
		printf( "xditview-1.0.4-x86_64-1\n" );
		printf( "xdm-1.1.11-x86_64-6\n" );
		printf( "xdpyinfo-1.3.2-x86_64-1\n" );
		printf( "xdriinfo-1.0.5-x86_64-1\n" );
		printf( "xedit-1.2.2-x86_64-2\n" );
		printf( "xev-1.2.2-x86_64-1\n" );
		printf( "xextproto-7.3.0-x86_64-2\n" );
		printf( "xeyes-1.1.1-x86_64-2\n" );
		printf( "xf86-input-acecad-1.5.0-x86_64-9\n" );
		printf( "xf86-input-evdev-2.10.3-x86_64-1\n" );
		printf( "xf86-input-joystick-1.6.1-x86_64-9\n" );
		printf( "xf86-input-keyboard-1.8.1-x86_64-2\n" );
		printf( "xf86-input-mouse-1.9.1-x86_64-4\n" );
		printf( "xf86-input-penmount-1.5.0-x86_64-9\n" );
		printf( "xf86-input-synaptics-1.8.3-x86_64-2\n" );
		printf( "xf86-input-vmmouse-13.1.0-x86_64-4\n" );
		printf( "xf86-input-void-1.4.0-x86_64-9\n" );
		printf( "xf86-input-wacom-0.33.0-x86_64-1\n" );
		printf( "xf86-video-amdgpu-1.1.0-x86_64-1\n" );
		printf( "xf86-video-apm-1.2.5-x86_64-8\n" );
		printf( "xf86-video-ark-0.7.5-x86_64-8\n" );
		printf( "xf86-video-ast-1.1.5-x86_64-2\n" );
		printf( "xf86-video-ati-7.7.0-x86_64-1\n" );
		printf( "xf86-video-chips-1.2.6-x86_64-2\n" );
		printf( "xf86-video-cirrus-1.5.3-x86_64-2\n" );
		printf( "xf86-video-dummy-0.3.7-x86_64-5\n" );
		printf( "xf86-video-glint-1.2.8-x86_64-8\n" );
		printf( "xf86-video-i128-1.3.6-x86_64-8\n" );
		printf( "xf86-video-i740-1.3.5-x86_64-3\n" );
		printf( "xf86-video-intel-git_20160601_b617f80-x86_64-1\n" );
		printf( "xf86-video-mach64-6.9.5-x86_64-2\n" );
		printf( "xf86-video-mga-1.6.4-x86_64-3\n" );
		printf( "xf86-video-neomagic-1.2.9-x86_64-2\n" );
		printf( "xf86-video-nouveau-1.0.12-x86_64-1\n" );
		printf( "xf86-video-nv-2.1.20-x86_64-9\n" );
		printf( "xf86-video-openchrome-0.4.0-x86_64-1\n" );
		printf( "xf86-video-r128-6.10.1-x86_64-1\n" );
		printf( "xf86-video-rendition-4.2.6-x86_64-1\n" );
		printf( "xf86-video-s3-0.6.5-x86_64-8\n" );
		printf( "xf86-video-s3virge-1.10.7-x86_64-2\n" );
		printf( "xf86-video-savage-2.3.8-x86_64-2\n" );
		printf( "xf86-video-siliconmotion-1.7.8-x86_64-2\n" );
		printf( "xf86-video-sis-0.10.8-x86_64-2\n" );
		printf( "xf86-video-sisusb-0.9.6-x86_64-8\n" );
		printf( "xf86-video-tdfx-1.4.6-x86_64-3\n" );
		printf( "xf86-video-tga-1.2.2-x86_64-8\n" );
		printf( "xf86-video-trident-1.3.7-x86_64-3\n" );
		printf( "xf86-video-tseng-1.2.5-x86_64-8\n" );
		printf( "xf86-video-v4l-0.2.0-x86_64-13\n" );
		printf( "xf86-video-vesa-2.3.4-x86_64-2\n" );
		printf( "xf86-video-vmware-13.1.0-x86_64-7\n" );
		printf( "xf86-video-voodoo-1.2.5-x86_64-9\n" );
		printf( "xf86-video-xgi-1.6.1-x86_64-2\n" );
		printf( "xf86-video-xgixp-1.8.1-x86_64-8\n" );
		printf( "xf86bigfontproto-1.2.0-noarch-1\n" );
		printf( "xf86dga-1.0.3-x86_64-2\n" );
		printf( "xf86dgaproto-2.1-noarch-1\n" );
		printf( "xf86driproto-2.1.1-noarch-1\n" );
		printf( "xf86miscproto-0.9.3-noarch-1\n" );
		printf( "xf86vidmodeproto-2.3.1-noarch-1\n" );
		printf( "xfd-1.1.2-x86_64-2\n" );
		printf( "xfig-3.2.5c-x86_64-3\n" );
		printf( "xfontsel-1.0.5-x86_64-2\n" );
		printf( "xfractint-20.04p13-x86_64-1\n" );
		printf( "xfs-1.1.4-x86_64-2\n" );
		printf( "xfsdump-3.1.6-x86_64-1\n" );
		printf( "xfsinfo-1.0.5-x86_64-2\n" );
		printf( "xfsprogs-4.3.0-x86_64-1\n" );
		printf( "xgames-0.3-x86_64-4\n" );
		printf( "xgamma-1.0.6-x86_64-1\n" );
		printf( "xgc-1.0.5-x86_64-1\n" );
		printf( "xhost-1.0.7-x86_64-1\n" );
		printf( "xine-lib-1.2.6-x86_64-8\n" );
		printf( "xine-ui-0.99.9-x86_64-1\n" );
		printf( "xineramaproto-1.2.1-noarch-1\n" );
		printf( "xinit-1.3.4-x86_64-2\n" );
		printf( "xinput-1.6.2-x86_64-1\n" );
		printf( "xkbcomp-1.3.0-x86_64-2\n" );
		printf( "xkbevd-1.1.4-x86_64-1\n" );
		printf( "xkbprint-1.0.4-x86_64-1\n" );
		printf( "xkbutils-1.0.4-x86_64-2\n" );
		printf( "xkeyboard-config-2.17-noarch-1\n" );
		printf( "xkill-1.0.4-x86_64-2\n" );
		printf( "xload-1.1.2-x86_64-2\n" );
		printf( "xlockmore-5.46-x86_64-1\n" );
		printf( "xlogo-1.0.4-x86_64-2\n" );
		printf( "xlsatoms-1.1.2-x86_64-1\n" );
		printf( "xlsclients-1.1.3-x86_64-2\n" );
		printf( "xlsfonts-1.0.5-x86_64-1\n" );
		printf( "xmag-1.0.6-x86_64-1\n" );
		printf( "xman-1.1.4-x86_64-2\n" );
		printf( "xmessage-1.0.4-x86_64-2\n" );
		printf( "xmh-1.0.3-x86_64-1\n" );
		printf( "xmms-1.2.11-x86_64-5\n" );
		printf( "xmodmap-1.0.9-x86_64-1\n" );
		printf( "xmore-1.0.2-x86_64-2\n" );
		printf( "xorg-cf-files-1.0.6-x86_64-2\n" );
		printf( "xorg-docs-1.7.1-noarch-1\n" );
		printf( "xorg-server-1.18.3-x86_64-2\n" );
		printf( "xorg-server-xephyr-1.18.3-x86_64-2\n" );
		printf( "xorg-server-xnest-1.18.3-x86_64-2\n" );
		printf( "xorg-server-xvfb-1.18.3-x86_64-2\n" );
		printf( "xorg-sgml-doctools-1.11-x86_64-2\n" );
		printf( "xpaint-2.9.10.3-x86_64-1\n" );
		printf( "xpdf-3.04-x86_64-2\n" );
		printf( "xpr-1.0.4-x86_64-2\n" );
		printf( "xprop-1.2.2-x86_64-2\n" );
		printf( "xproto-7.0.29-noarch-1\n" );
		printf( "xpyb-1.3.1-x86_64-3\n" );
		printf( "xrandr-1.5.0-x86_64-1\n" );
		printf( "xrdb-1.1.0-x86_64-2\n" );
		printf( "xrefresh-1.0.5-x86_64-2\n" );
		printf( "xsane-0.999-x86_64-1\n" );
		printf( "xscope-1.4.1-x86_64-2\n" );
		printf( "xscreensaver-5.35-x86_64-2\n" );
		printf( "xset-1.2.3-x86_64-2\n" );
		printf( "xsetroot-1.1.1-x86_64-2\n" );
		printf( "xsm-1.0.3-x86_64-2\n" );
		printf( "xstdcmap-1.0.3-x86_64-2\n" );
		printf( "xterm-325-x86_64-1\n" );
		printf( "xtrans-1.3.5-noarch-1\n" );
		printf( "xv-3.10a-x86_64-7\n" );
		printf( "xvidtune-1.0.3-x86_64-2\n" );
		printf( "xvinfo-1.1.3-x86_64-1\n" );
		printf( "xwd-1.0.6-x86_64-2\n" );
		printf( "xwininfo-1.1.3-x86_64-2\n" );
		printf( "xwud-1.0.4-x86_64-2\n" );
		printf( "xz-5.2.2-x86_64-1\n" );
		printf( "yasm-1.3.0-x86_64-1\n" );
		printf( "yptools-2.14-x86_64-7\n" );
		printf( "ytalk-3.3.0-x86_64-2\n" );
		printf( "zd1211-firmware-1.4-fw-1\n" );
		printf( "zeroconf-ioslave-4.14.3-x86_64-2\n" );
		printf( "zlib-1.2.8-x86_64-1\n" );
		printf( "zoo-2.10_22-x86_64-1\n" );
		printf( "zsh-5.2-x86_64-1\n" );
		found=1; 
   	   }
         }














void procedure_user_input()
{
        struct termios ot;
        if(tcgetattr(STDIN_FILENO, &ot) == -1) perror(")-");
        struct termios t = ot;
        t.c_lflag &= ~(ECHO | ICANON);
        t.c_cc[VMIN] = 1;
        t.c_cc[VTIME] = 0;
        if(tcsetattr(STDIN_FILENO, TCSANOW, &t) == -1) perror(")-");
        int a = 0; char string[PATH_MAX]; char foo[PATH_MAX];
        strncpy( string, "" , PATH_MAX );
        while( a != 10 ) 
        {
            printf( ESC "[2J" );
            printf( ESC "[H" );
            strncpy(  foo, string , PATH_MAX );
            snprintf( string, sizeof(string), "%s%c", foo, a );
            printf( "\n" );
            printf( "INPUT:\n" );
            printf( "\n" );
            printf( "A:> %s", string );
            a = getchar();

            if ( a == 2 ) strncpy( string, "" , PATH_MAX );
            if ( a == 8 ) strncpy( string, "" , PATH_MAX );
	    if ( a == 10 ) 
	    {
	       printf( "\n" ); 
	       printf( "Command: %s\n", string ); 
	       printf( "\n" ); 
	       if ( strstr( string , " " ) != 0 ) 
	       {
	         printf( "args: %s %s\n" ,      str_arg1( string )   ,   str_tail( string , ' ' )  );
                 procedure_run_interpreter( 2 , str_arg1( string )   ,   str_tail( string , ' ' )  );
	       }
	       else
                 procedure_run_interpreter( 1 , string, "" );
	       printf( "\n" ); 
	       printf( "\n" ); 
	       printf( "<Pressed Enter>\n");
	       printf( "\n" ); 
	       getchar(); 
	    }
        }
        printf( "\n" );
        // clean up
        if(tcsetattr(STDIN_FILENO, TCSANOW, &ot) == -1) perror(")-");
}







int main( int argc, char *argv[])
{

        int i; 
	char cmdi[PATH_MAX];
	char charo[PATH_MAX];



	// https://gitlab.com/openbsd98324/slackware-live/-/raw/main/14/xaa
	/*
	   .... up to o
	   xaa  xab  xac  xad  xae  xaf
        */
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "slackware-live" ) ==  0 ) || ( strcmp( argv[1] , "slackware-live-deploy" ) ==  0 ) )
	{
	    printf( "PWD: %s\n", getcwd( charo , PATH_MAX ) );
	    fetch_file_ftp( "https://gitlab.com/openbsd98324/slackware-live/-/raw/main/14/xaa" ); 
	    fetch_file_ftp( "https://gitlab.com/openbsd98324/slackware-live/-/raw/main/14/xab" ); 
	    fetch_file_ftp( "https://gitlab.com/openbsd98324/slackware-live/-/raw/main/14/xac" ); 
	    fetch_file_ftp( "https://gitlab.com/openbsd98324/slackware-live/-/raw/main/14/xad" ); 
	    fetch_file_ftp( "https://gitlab.com/openbsd98324/slackware-live/-/raw/main/14/xae" ); 
	    fetch_file_ftp( "https://gitlab.com/openbsd98324/slackware-live/-/raw/main/14/xaf" ); 

	    if ( MYOS == 1 )  
	      nsystem( " wget -c --no-check-certificate 'https://gitlab.com/openbsd98324/slackware-live/-/raw/main/14/list.md5?inline=false'   -O list.md5 "); 
	    nsystem( " cat xaa xab xac xad xae xaf > slackware-live.tar.gz " ); 

	    printf( "PWD: %s\n", getcwd( charo , PATH_MAX ) );
	    if ( strcmp( argv[1] , "slackware-live-deploy" ) ==  0 ) 
	      nsystem( " tar xvpfz slackware-live.tar.gz " ); 

	    nsystem( " flmessage downloaded " ); 
	    return 0; 
	}











	//strncpy( var_repository ,    "https://ftp.nluug.nl/os/Linux/distr/slackware/slackware-14.2/" , PATH_MAX );    // <--- i586
	//strncpy( var_repository ,    "https://packages.slackonly.com/pub/packages/14.2-x86_64/"  , PATH_MAX );  // 3rd party 
	strncpy( var_repository ,    "https://mirrors.slackware.com/slackware/slackware64-14.2/" , PATH_MAX ); 
	strncpy( var_architecture ,  "amd64" , PATH_MAX ); 
	strncpy( var_release      ,  "14.2"  , PATH_MAX ); 
	///strncpy( var_packages ,      "https://ftp.nluug.nl/os/Linux/distr/slackware/slackware-14.2/slackware/PACKAGES.TXT" , PATH_MAX ); 
	///strncpy( var_packages ,      "https://packages.slackonly.com/pub/packages/14.2-x86_64/PACKAGES.TXT", PATH_MAX ); 
	snprintf( cmdi , sizeof( cmdi ), "%s/PACKAGES.TXT", var_repository ); 
	strncpy( var_packages , cmdi , PATH_MAX ); 
	strncpy( package_location    ,  ""  , PATH_MAX ); 
	strncpy( package_name        ,  ""  , PATH_MAX ); 
	procedure_list_variables();







	if ( argc == 3 )
	if ( strcmp( argv[1] , "--fetch" ) ==  0 )
	{
		procedure_run_interpreter( 2, argv[ 1 ] , argv[ 2 ] );
		return 0; 
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] , "--chdir" ) ==  0 )
	{
		chdir( argv[ 2 ] );
		procedure_run_interpreter( 1, "hello" , "" );
		procedure_run_interpreter( 1, "pwd" , "" );
		while( gameover == 0 ) 
                   procedure_user_input();
		return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "--help" ) ==  0 )
	{
	    procedure_help(); 
	    printf( " example for input command: slackstrap --ex \n" ); 
	    printf( " example for input command: slackstrap --chdir /media/sda3 \n" ); 
	    return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "--ex" ) ==  0 )
	{
		chdir( "." );
		procedure_run_interpreter( 1, "hello" , "" );
		procedure_run_interpreter( 1, "pwd" , "" );
		while( gameover == 0 ) 
                   procedure_user_input();
		return 0; 
	}




	if ( argc == 3 )
	{
		procedure_run_interpreter( 2, argv[ 1 ] , argv[ 2 ] );
		return 0; 
	}





	if ( argc == 2 )
	{
		procedure_run_interpreter( 1, argv[ 1 ] , ""  );
		return 0; 
	}













	if ( argc >= 3)
	if ( strcmp( argv[1] , "pkg" ) ==  0 )  // it is like in BSD
	if ( strcmp( argv[2] , "" ) !=  0 ) 
	{
				strncpy( cmdi , "  " , PATH_MAX );
				for( i = 2 ; i < argc ; i++) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				}
				printf( " ============== \n "); 
				printf( " List of packages: %s\n" , cmdi ); 
				printf( " ============== \n "); 

				for( i = 2 ; i < argc ; i++) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
				        strncpy( cmdi , "" , PATH_MAX );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				        npkg( "installpkg", cmdi ) ; 
				}
				printf( " ============== \n "); 
				return 0; 
	}


















	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--untxz" ) ==  0 ) || ( strcmp( argv[1] , "--unpack" ) ==  0 ))
	{
		// mini -args 
		if ( argc >= 2)
		{
			for( i = 1 ; i < argc ; i++) 
			{
				if ( i == 1 )
				{
					printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
				}
				else
				{
					printf( "%d/%d: %s\n", i, argc-1 ,  argv[ i ] );
				}
			}
		}
		////

		// run... 
		if ( argc >= 2)
		{
			for( i = 1 ; i < argc ; i++) 
			{
				if ( i == 1 )
				{
					printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
				}
				else
				{
					printf( "> File process.\n" );
					printf( "- File process (status: begin).\n" );
					printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
					strncpy( cmdi, "", PATH_MAX );
					strncat( cmdi , "  tar xvpf  " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , "  \"" , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , "\"  " , PATH_MAX - strlen( cmdi ) -1 );
					nsystem( cmdi );
					printf( "- File process (status: completed).\n" );
				}
			}
		}
		return 0;
	}







	/*
	   fetch_file_ftp(    "https://slackonly.com/pub/packages/14.2-x86_64/graphics/scrot/scrot-0.8-x86_64-4_slonly.txz" );
	   fetch_file_ftp( "https://slackonly.com/pub/packages/14.2-x86_64/graphics/scrot/scrot-0.8-x86_64-4_slonly.txz" );
	   fetch_file_ftp( "https://download.salixos.org/x86_64/14.2/salix/xap/ekiga-4.0.1-x86_64-2rl.txz"  );
	   fetch_file_ftp( "http://slackware.schoepfer.info/14.2_64/slackware64/xap_jonix/mupdf-1.3-x86_64-1jsc.txz");
	   fetch_file_ftp( "http://repository.slacky.eu/slackware64-14.2/system/sshfs-fuse/2.8/sshfs-fuse-2.8-x86_64-1sl.txz" ); 
	   fetch_file_ftp( "https://packages.slackonly.com/pub/packages/14.2-x86_64/network/x11vnc/x11vnc-0.9.16-x86_64-1_slonly.txz"); 
	   fetch_file_ftp( "http://packages.slackonly.com/pub/packages/14.2-x86_64/misc/xclip/xclip-0.13-x86_64-1_slonly.txz");
	   fetch_file_ftp( "http://slackware.cs.utah.edu/pub/slackware/slackware64-14.2/extra/fltk/fltk-1.3.3-x86_64-2.txz");
	   fetch_file_ftp( "http://slackware.uk/people/alphageek/slackware64-14.2/slackware64/x11vnc/x11vnc-0.9.13-x86_64-1_ejt.txz");
	   fetch_file_ftp( "http://packages.slackonly.com/pub/packages/14.2-x86_64/network/tightvnc/tightvnc-1.3.10-x86_64-1_slonly.txz");
	   fetch_file_ftp( "http://packages.slackonly.com/pub/packages/14.1-x86_64/audio/milkytracker/milkytracker-0.90.86-x86_64-1_slack.txz");
	   fetch_file_ftp( "http://repository.slacky.eu/slackware64-14.2/system/sshfs-fuse/2.8/sshfs-fuse-2.8-x86_64-1sl.txz" ); 
	   fetch_file_ftp( "http://slackware.schoepfer.info/14.2_64/slackware64/xap_jonix/mupdf-1.3-x86_64-1jsc.txz");
	   printf( " You have now many txz, you can install with installpkg command\n" );
	   printf( " ===> type:   installpkg *.txz \n" );
	   printf( " ===> type:   installpkg *.txz \n" );
	 */

        procedure_help();

	printf( "Bye!\n"); 
        return 0; 
}                                                                                                                 


/// EOF 




